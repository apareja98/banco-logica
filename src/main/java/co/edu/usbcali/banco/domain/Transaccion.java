package co.edu.usbcali.banco.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the transaccion database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Transaccion.findAll", query="SELECT t FROM Transaccion t"),
	@NamedQuery(name="Transaccion.findDTO", query="Select new co.edu.usbcali.banco.dto.TransaccionDTO(t.tranId,t.fecha, t.valor,t.cuenta.cuenId, t.tipoTransaccion.nombre,t.usuario.usuUsuario,t.usuario.tipoUsuario.nombre) FROM Transaccion t")
})
public class Transaccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="tran_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tranId;
	@PastOrPresent
	@NotNull
	private Timestamp fecha;
	@Positive
	@NotNull
	private BigDecimal valor;

	//bi-directional many-to-one association to Cuenta
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cuen_id")
	@NotNull
	private Cuenta cuenta;

	//bi-directional many-to-one association to TipoTransaccion
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="titr_id")
	@NotNull
	private TipoTransaccion tipoTransaccion;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="usu_usuario")
	@NotNull
	private Usuario usuario;

	public Transaccion() {
	}

	public Long getTranId() {
		return this.tranId;
	}

	public void setTranId(Long tranId) {
		this.tranId = tranId;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Cuenta getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public TipoTransaccion getTipoTransaccion() {
		return this.tipoTransaccion;
	}

	public void setTipoTransaccion(TipoTransaccion tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}