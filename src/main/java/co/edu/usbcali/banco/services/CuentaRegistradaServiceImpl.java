package co.edu.usbcali.banco.services;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.dto.CuentaRegistradaDTO;
import co.edu.usbcali.banco.repository.CuentaRegistradaRepository;

@Service
@Scope("singleton")
public class CuentaRegistradaServiceImpl implements CuentaRegistradaService {

	@Autowired
	private CuentaRegistradaRepository cuentaRegistradaRepository;

	@Autowired
	private Validator validator;

	@Autowired
	private CuentaService cuentaService;
	@Autowired
	private ClienteService clienteService;
	
	public void validarCuentaRegistrada(CuentaRegistrada cuentaRegistrada) throws Exception {
		try {
			Set<ConstraintViolation<CuentaRegistrada>> constraintViolations = validator.validate(cuentaRegistrada);

			if (constraintViolations.size() > 0) {
				StringBuilder strMessage = new StringBuilder();

				for (ConstraintViolation<CuentaRegistrada> constraintViolation : constraintViolations) {
					strMessage.append(constraintViolation.getPropertyPath().toString());
					strMessage.append(" - ");
					strMessage.append(constraintViolation.getMessage());
					strMessage.append(". \n");
				}

				throw new Exception(strMessage.toString());
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(CuentaRegistrada cuentaRegistrada) throws Exception {
		if (cuentaRegistrada == null)
			throw new Exception("La cuenta registrada es nula y no debe serlo");
		validarCuentaRegistrada(cuentaRegistrada);
		if (findById(cuentaRegistrada.getCureId()) != null)
			throw new Exception("Ya hay una cuenta registrada con el id =" + cuentaRegistrada.getCureId().toString());
		cuentaRegistradaRepository.save(cuentaRegistrada);

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void update(CuentaRegistrada cuentaRegistrada) throws Exception {
		if (cuentaRegistrada == null)
			throw new Exception("La cuenta registrada es nula y no debe serlo");
		validarCuentaRegistrada(cuentaRegistrada);
		cuentaRegistradaRepository.update(cuentaRegistrada);

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(CuentaRegistrada cuentaRegistrada) throws Exception {
		if (cuentaRegistrada == null)
			throw new Exception("La cuenta registrada es nula y no debe serlo");
		cuentaRegistrada = findById(cuentaRegistrada.getCureId());
		cuentaRegistradaRepository.delete(cuentaRegistrada);
		
		

	}

	@Override
	@Transactional(readOnly = true)
	public CuentaRegistrada findById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return cuentaRegistradaRepository.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CuentaRegistrada> findAll() throws Exception {
		// TODO Auto-generated method stub
		return cuentaRegistradaRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<CuentaRegistradaDTO> findAllDTO() throws Exception {
		// TODO Auto-generated method stub
		return cuentaRegistradaRepository.findAllDTO();
	}

	@Override
	@Transactional(readOnly = true, propagation=Propagation.REQUIRES_NEW)
	public Boolean esCuentaRegistrada(String cuenId, Long clieId) throws Exception {
		List<CuentaRegistrada> cuentaRegistradas = cuentaRegistradaRepository.cuentaRegistradaCliente(cuenId, clieId);
		if(cuentaRegistradas == null || cuentaRegistradas.isEmpty())return false;
		return true;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void registrarCuenta(Long clieId, Long tiDocId, Long id, String cuenId) throws Exception {
		Cuenta cuenta = cuentaService.findById(cuenId);
		if(cuenta==null)throw new Exception("La cuenta no existe");
		if(cuenta.getActiva().equalsIgnoreCase("N"))throw new Exception("La cuenta debe estar activa");
		Cliente cliente = clienteService.findById(clieId);
		
		if(cliente==null) throw new Exception("El cliente no existe");
		Cliente clienteCuenta = cuenta.getCliente();
		if(!(clienteCuenta.getClieId()==id && clienteCuenta.getTipoDocumento().getTdocId()==tiDocId)) {
			throw new Exception("Los datos del titular de la cuenta no son correctos");
		}
		if(esCuentaRegistrada(cuenId, clieId))throw new Exception("Ya registraste esta cuenta");
		CuentaRegistrada cuentaRegistrada = new CuentaRegistrada();
		cuentaRegistrada.setCliente(cliente);
		cuentaRegistrada.setCuenta(cuenta);
		cuentaRegistrada.setCureId(null);
		cuentaRegistradaRepository.save(cuentaRegistrada);
	}

	@Override
	@Transactional(readOnly=true)
	public List<CuentaRegistrada> cuentasRegistradasPorCliente(Long clieId) throws Exception {
		
		return cuentaRegistradaRepository.cuentasRegistradasPorCliente(clieId);
	}

}
