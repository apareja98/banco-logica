package co.edu.usbcali.banco.services;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.repository.TipoUsuarioRepository;

@Service
@Scope("singleton")
public class TipoUsuarioServiceImpl implements TipoUsuarioService {
	@Autowired
	TipoUsuarioRepository tipoUsuarioRepository;

	@Autowired
	private Validator validator;

	public void validarTipoUsuario(TipoUsuario tipoUsuario) throws Exception {
		try {
			Set<ConstraintViolation<TipoUsuario>> constraintViolations = validator.validate(tipoUsuario);

			if (constraintViolations.size() > 0) {
				StringBuilder strMessage = new StringBuilder();

				for (ConstraintViolation<TipoUsuario> constraintViolation : constraintViolations) {
					strMessage.append(constraintViolation.getPropertyPath().toString());
					strMessage.append(" - ");
					strMessage.append(constraintViolation.getMessage());
					strMessage.append(". \n");
				}

				throw new Exception(strMessage.toString());
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(TipoUsuario tipoUsuario) throws Exception {
		if (tipoUsuario == null)
			throw new Exception("El tipo de usuario es nulo y no debe serlo");
		validarTipoUsuario(tipoUsuario);
		if (findById(tipoUsuario.getTiusId()) != null)
			throw new Exception("Ya hay un tipo de usuario con este id");
		tipoUsuarioRepository.save(tipoUsuario);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void update(TipoUsuario tipoUsuario) throws Exception {
		if (tipoUsuario == null)
			throw new Exception("El tipo de usuario es nulo y no debe serlo");
		validarTipoUsuario(tipoUsuario);
		tipoUsuarioRepository.update(tipoUsuario);

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(TipoUsuario tipoUsuario) throws Exception {
		if (tipoUsuario == null)
			throw new Exception("El tipo de usuario es nulo y no debe serlo");
		tipoUsuario = findById(tipoUsuario.getTiusId());
		if (tipoUsuario.getUsuarios() != null && tipoUsuario.getUsuarios().size() > 0)
			throw new Exception("Este tipo de usuario está siendo utilizado por algunos usuarios, no se puede borrar");
		tipoUsuarioRepository.delete(tipoUsuario);
	}

	@Override
	@Transactional(readOnly = true)
	public TipoUsuario findById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return tipoUsuarioRepository.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoUsuario> findAll() throws Exception {
		// TODO Auto-generated method stub
		return tipoUsuarioRepository.findAll();
	}

}
