package co.edu.usbcali.banco.services;

import java.util.List;


import co.edu.usbcali.banco.domain.Cuenta;

public interface CuentaService {
	
	public void save(Cuenta cuenta)throws Exception ;
	
	public void update(Cuenta cuenta)throws Exception ;
	
	public void delete(Cuenta cuenta)throws Exception ;
	
	public Cuenta findById(String id)throws Exception ;
	
	public List<Cuenta> findAll()throws Exception ;
	
	public void inactivar(Cuenta cuenta)throws Exception;
	
	public List<Cuenta> cuentasPorCliente(Long idCliente)throws Exception;
	
	public void cambiarClaveCuenta(String cuenId, String antigua, String nueva)throws Exception;
	
	public List<Cuenta> cuentasRegistradasPorCliente(Long idCliente) throws Exception;
	
	
}
