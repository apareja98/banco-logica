package co.edu.usbcali.banco.services;

import java.math.BigDecimal;

import co.edu.usbcali.banco.domain.TipoTransaccion;

public interface EnviarCorreoService {
	public void enviarCorreoCreacionCuenta(String correo,  String cuenId, String clave )throws Exception;
	public void enviarCorreoTransaccion(String correo, BigDecimal valor, String cuen_id, TipoTransaccion tipoTransaccion) throws Exception;
	public void enviarCorreoCambioDeClave (String correo,String cuenId, String clave)throws Exception;
}
