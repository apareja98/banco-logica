package co.edu.usbcali.banco.services;

import java.util.List;


import co.edu.usbcali.banco.domain.TipoDocumento;

public interface TipoDocumentoService {
	
	public void save(TipoDocumento  tipoDocumento)throws Exception;
	
	public void update(TipoDocumento tipoDocumento)throws Exception;
	
	public void delete(TipoDocumento  tipoDocumento)throws Exception;
	
	public TipoDocumento findById(Long id)throws Exception;
	
	public List<TipoDocumento> findAll()throws Exception;
	



}
