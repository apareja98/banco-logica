package co.edu.usbcali.banco.services;

import java.util.List;

import co.edu.usbcali.banco.domain.Cliente;

public interface ClienteService {
	
	public void save(Cliente cliente) throws Exception;

	public void update(Cliente cliente) throws Exception;

	public void delete(Cliente cliente) throws Exception;

	public Cliente findById(Long id) throws Exception;

	public List<Cliente> findAll() throws Exception;
	
	public  Cliente loginCliente(String cuenId, Long id, String clave)throws Exception;
	
	public Cliente consultarCliente(String cuenId, Long clieId, Long tiDocId)throws Exception;
	


}
