package co.edu.usbcali.banco.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.domain.TipoTransaccion;
import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.repository.CuentaRegistradaRepository;

@Service
@Scope("singleton")
public class TransaccionBancariaServiceImpl implements TransaccionBancariaService {
	
	private final static Long TIPO_RETIRO = 1L;
	private final static Long TIPO_CONSIGNACION = 2L;
	private final static Long TIPO_TRANSFERENCIA = 3L;
	private final static Long CAJERO = 1L;
	private final static Long PORTAL_WEB = 4L;
	private final static String CUENTA_BANCO="9999-9999-9999-9999";
	private final static BigDecimal costoTransferencia = new BigDecimal(2000);

	
	
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private TransaccionService transaccionService;
	@Autowired
	private CuentaService cuentaService;
	@Autowired
	private TipoTransaccionService tipoTransaccionService;
	@Autowired
	private EnviarCorreoService enviarCorreoService;
	@Autowired
	private CuentaRegistradaService cuentaRegistradaService;
	@Autowired 
	private CuentaRegistradaRepository cuentaRegistradaRepository;
	
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public Cuenta consignar(String cuenId, String usuUsuario, BigDecimal valor) throws Exception {
		Cuenta cuenta=cuentaService.findById(cuenId);
		Usuario usuario=usuarioService.findById(usuUsuario);
		
		if(cuenta==null) {
			throw new Exception("La cuenta numero:"+cuenId+" No existe");
		}
		if(cuenta.getActiva().equalsIgnoreCase("N")==true && cuenta.getTransaccions().size()>0) {
			throw new Exception("La cuenta numero:"+cuenId+" esta inactiva");
		}
		if(usuario==null) {
			throw new Exception("El usuario:"+usuUsuario+" No existe");
		}
		if(usuario.getActivo().equalsIgnoreCase("N")==true) {
			throw new Exception("El usuario:"+usuUsuario+" esta inactivo");
		}
		if(usuario.getTipoUsuario().getTiusId()!=CAJERO && usuario.getTipoUsuario().getTiusId() != PORTAL_WEB) {
			throw new Exception("El usuario:"+usuUsuario+" no tiene permisos");
		}
		
		if(valor.floatValue()<=0) {
			throw new Exception("El valor debe ser mayor a Cero");
		}
		
		if(cuenta.getActiva().equalsIgnoreCase("N")==true && cuenta.getTransaccions().size()==0 ) {
			if(!(valor.compareTo(new BigDecimal("200000"))==1 || valor.compareTo(new BigDecimal("200000"))==0)) {
				throw new Exception("Para la apertura y activación de una cuenta se debe consignar $200.000 o más");
			}
			cuenta.setActiva("S");
		}
		TipoTransaccion tipoTransaccion=tipoTransaccionService.findById(TIPO_CONSIGNACION);
		
		Transaccion transaccion =new Transaccion();
		transaccion.setTranId(null);
		transaccion.setCuenta(cuenta);
		transaccion.setFecha(new Timestamp(System.currentTimeMillis()));
		transaccion.setTipoTransaccion(tipoTransaccion);
		transaccion.setUsuario(usuario);
		transaccion.setValor(valor);
		cuenta.setSaldo(cuenta.getSaldo().add(valor));		
		transaccionService.save(transaccion);
		cuentaService.update(cuenta);	
		enviarCorreoService.enviarCorreoTransaccion(cuenta.getCliente().getEmail(), valor, cuenta.getCuenId(), tipoTransaccion);
		return cuenta;
	}
	
	//TODO retirarDesdeCajeroConClave
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public Cuenta retirar(String cuenId, String usuUsuario, BigDecimal valor) throws Exception {
		Cuenta cuenta=cuentaService.findById(cuenId);
		Usuario usuario=usuarioService.findById(usuUsuario);
		if(cuenta==null) {
			throw new Exception("La cuenta numero:"+cuenId+" No existe");
		}
		if(cuenta.getActiva().equalsIgnoreCase("N")==true) {
			throw new Exception("La cuenta numero:"+cuenId+" esta inactiva");
		}
		if(usuario==null) {
			throw new Exception("El usuario:"+usuUsuario+" No existe");
		}
		if(usuario.getActivo().equalsIgnoreCase("N")==true) {
			throw new Exception("El usuario:"+usuUsuario+" esta inactivo");
		}
		if(usuario.getTipoUsuario().getTiusId()!=CAJERO && usuario.getTipoUsuario().getTiusId() != PORTAL_WEB) {
			throw new Exception("El usuario:"+usuUsuario+" no tiene permisos");
		}
		if(valor.floatValue()<=0) {
			throw new Exception("El valor debe ser positivo mayor a Cero");
		}
		if(cuenta.getSaldo().floatValue()<valor.floatValue()) {
			throw new Exception("El saldo es insuficiente para realizar el retiro");
		}
		TipoTransaccion tipoTransaccion=tipoTransaccionService.findById(TIPO_RETIRO);
		Transaccion transaccion =new Transaccion();
		transaccion.setTranId(null);
		transaccion.setCuenta(cuenta);
		transaccion.setFecha(new Timestamp(System.currentTimeMillis()));
		transaccion.setTipoTransaccion(tipoTransaccion);
		transaccion.setUsuario(usuario);
		transaccion.setValor(valor);
		//Resta el saldo
		cuenta.setSaldo(cuenta.getSaldo().subtract(valor));		
		transaccionService.save(transaccion);
		cuentaService.update(cuenta);	
		enviarCorreoService.enviarCorreoTransaccion(cuenta.getCliente().getEmail(), valor, cuenta.getCuenId(), tipoTransaccion);
		return cuenta;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public Cuenta traslado(String cuenIdOrigen, String cuenIdDestino, String usuUsuario, BigDecimal valor)throws Exception {
		
		Cuenta cuenta = cuentaService.findById(cuenIdOrigen);
		
		if(cuentaRegistradaService.esCuentaRegistrada(cuenIdDestino,cuenta.getCliente().getClieId())==false) {
			throw new Exception("La cuenta " + cuenIdDestino + "  no está registrada, debes registrarla");
		}
		
	
		
		Usuario usuario=usuarioService.findById(usuUsuario);
		
		if(usuario==null) {
			throw new Exception("El usuario:"+usuUsuario+" No existe");
		}
		
		if(usuario.getActivo().equalsIgnoreCase("N")==true) {
			throw new Exception("El usuario:"+usuUsuario+" esta inactivo");
		}
		//Tener presente el portal
		if(usuario.getTipoUsuario().getTiusId()!=CAJERO && usuario.getTipoUsuario().getTiusId() != PORTAL_WEB) {
			throw new Exception("El usuario:"+usuUsuario+" no tiene permisos");
		}
		
		retirar(cuenIdOrigen, usuUsuario, valor);
		Cuenta cuentaOrigen=retirar(cuenIdOrigen, usuUsuario, costoTransferencia);
		consignar(cuenIdDestino, usuUsuario, valor);
		consignar(CUENTA_BANCO, usuUsuario, costoTransferencia);
		
		TipoTransaccion tipoTransaccion=tipoTransaccionService.findById(TIPO_TRANSFERENCIA);
		
		Transaccion transaccion =new Transaccion();
		transaccion.setTranId(null);
		transaccion.setCuenta(cuentaOrigen);
		transaccion.setFecha(new Timestamp(System.currentTimeMillis()));
		transaccion.setTipoTransaccion(tipoTransaccion);
		transaccion.setUsuario(usuario);
		transaccion.setValor(valor);
		transaccionService.save(transaccion);
		enviarCorreoService.enviarCorreoTransaccion(cuenta.getCliente().getEmail(), valor, cuenta.getCuenId(), tipoTransaccion);
	
		return cuentaOrigen;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public Cuenta retirarATM(String cuenId, String clave, String usuUsuario, BigDecimal valor) throws Exception {
		Cuenta cuenta = cuentaService.findById(cuenId);
		if(cuenta==null) {
			throw new Exception("Credenciales erroneas, intentelo de nuevo");
		}
		if(!(cuenta.getClave().trim().equals(clave.trim())))throw new Exception("Credenciales erroneas, intentelo de nuevo");
		
		return retirar(cuenId, usuUsuario, valor);
	}

	@Override
	@Transactional(readOnly=true)
	public Cuenta consultaSaldo(String cuenId, String clave) throws Exception {
		Cuenta cuenta = cuentaService.findById(cuenId);
		if(cuenta==null) {
			throw new Exception("Credenciales erroneas, intentelo de nuevo");
		}
		if(!(cuenta.getClave().trim().equals(clave.trim())))throw new Exception("Credenciales erroneas, intentelo de nuevo");
		
		return cuenta;
		
	}

}
