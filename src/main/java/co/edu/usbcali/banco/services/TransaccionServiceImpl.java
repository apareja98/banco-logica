package co.edu.usbcali.banco.services;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.dto.TransaccionDTO;
import co.edu.usbcali.banco.repository.TransaccionRepository;

@Service
@Scope("singleton")
public class TransaccionServiceImpl implements TransaccionService {
	
	@Autowired
	private TransaccionRepository transaccionRepository;
	
	@Autowired
	private Validator validator;

	public void validarTransaccion(Transaccion transaccion) throws Exception {
		try {
			Set<ConstraintViolation<Transaccion>> constraintViolations = validator.validate(transaccion);

			if (constraintViolations.size() > 0) {
				StringBuilder strMessage = new StringBuilder();

				for (ConstraintViolation<Transaccion> constraintViolation : constraintViolations) {
					strMessage.append(constraintViolation.getPropertyPath().toString());
					strMessage.append(" - ");
					strMessage.append(constraintViolation.getMessage());
					strMessage.append(". \n");
				}

				throw new Exception(strMessage.toString());
			}
		} catch (Exception e) {
			throw e;
		}
	}

	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Transaccion save(Transaccion transaccion) throws Exception {
		if(transaccion==null)
			throw new Exception("La transacción es nula y no debe serlo");
		validarTransaccion(transaccion);

		transaccionRepository.save(transaccion);
		return transaccion;
		
	
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void update(Transaccion transaccion) throws Exception {
		if(transaccion==null)
			throw new Exception("La transacción es nula y no debe serlo");
		validarTransaccion(transaccion);
		transaccionRepository.update(transaccion);

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(Transaccion transaccion) throws Exception {
		if(transaccion==null)
			throw new Exception("La transacción es nula y no debe serlo");
		transaccion = findById(transaccion.getTranId());
		transaccionRepository.delete(transaccion);

	}

	@Override
	@Transactional(readOnly=true)
	public Transaccion findById(Long id) throws Exception {
		return transaccionRepository.findById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Transaccion> findAll() throws Exception {
		return transaccionRepository.findAll();
	}


	@Override
	@Transactional(readOnly=true)
	public List<TransaccionDTO> findAllDTO() throws Exception {
		return transaccionRepository.findAllDTO();
	}


	@Override
	@Transactional(readOnly=true)
	public List<Transaccion> transaccionesPorCliente(Long idCliente) throws Exception {
		return transaccionRepository.ultimasTransaccionesPorCliente(idCliente);
	}


	@Override
	@Transactional(readOnly=true)
	public List<Transaccion> transaccionesPorCuenta(String cuenId) throws Exception {
		
		return transaccionRepository.transaccionesPorCuenta(cuenId);
	}

}
