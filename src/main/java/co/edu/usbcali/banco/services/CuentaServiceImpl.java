package co.edu.usbcali.banco.services;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.repository.CuentaRepository;

@Service
@Scope("singleton")
public class CuentaServiceImpl implements CuentaService {

	@Autowired
	private CuentaRepository cuentaRepository;

	@Autowired
	private Validator validator;
	
	@Autowired
	private CuentaRegistradaService cuentaRegistradaService;
	@Autowired 
	private EnviarCorreoService enviarCorreoService;
	
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();

	public String randomString( int len ){
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
	   return sb.toString();
	}
	
	public void validarCuenta(Cuenta cuenta) throws Exception {
		try {
			Set<ConstraintViolation<Cuenta>> constraintViolations = validator.validate(cuenta);

			if (constraintViolations.size() > 0) {
				StringBuilder strMessage = new StringBuilder();

				for (ConstraintViolation<Cuenta> constraintViolation : constraintViolations) {
					strMessage.append(constraintViolation.getPropertyPath().toString());
					strMessage.append(" - ");
					strMessage.append(constraintViolation.getMessage());
					strMessage.append(". \n");
				}

				throw new Exception(strMessage.toString());
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(Cuenta cuenta) throws Exception {
		if (cuenta == null)
			throw new Exception("La cuenta es nula y no debe serlo");
		cuenta.setClave(randomString(7));
		validarCuenta(cuenta);
//		if (findById(cuenta.getCuenId()) != null)
//			throw new Exception("Ya hay una cuenta registrada con este id");
		cuentaRepository.save(cuenta);
		enviarCorreoService.enviarCorreoCreacionCuenta(cuenta.getCliente().getEmail(), cuenta.getCuenId(), cuenta.getClave());
		

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void update(Cuenta cuenta) throws Exception {
		if (cuenta == null)
			throw new Exception("La cuenta es nula y no debe serlo");
		validarCuenta(cuenta);
		cuentaRepository.update(cuenta);

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(Cuenta cuenta) throws Exception {
		if (cuenta == null)
			throw new Exception("La cuenta es nula y no debe serlo");
		cuenta = findById(cuenta.getCuenId());
		if (cuenta.getTransaccions() != null && cuenta.getTransaccions().size() > 0)
			throw new Exception("Esta cuenta tiene transacciones asociadas, no se puede borrar");
		if (cuenta.getCuentaRegistradas() != null && cuenta.getCuentaRegistradas().size() > 0)
			throw new Exception("Esta cuenta tiene cuentas registradas asociadas, no se puede borrar");
		cuentaRepository.delete(cuenta);
	}

	@Override
	@Transactional(readOnly=true)
	public Cuenta findById(String id) throws Exception {
		// TODO Auto-generated method stub
		return cuentaRepository.findById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Cuenta> findAll() throws Exception {
		// TODO Auto-generated method stub
		return cuentaRepository.findAll();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void inactivar(Cuenta cuenta) throws Exception {
		if (cuenta == null)
			throw new Exception("La cuenta es nula y no debe serlo");
		cuenta.setActiva("N");
		cuentaRepository.update(cuenta);
		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Cuenta> cuentasPorCliente(Long idCliente) throws Exception {
		return cuentaRepository.cuentasPorCliente(idCliente);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void cambiarClaveCuenta(String cuenId, String antigua, String nueva) throws Exception {
		Cuenta cuenta = findById(cuenId);
		if(cuenta==null)throw new Exception("La cuenta no existe");
		if(!(cuenta.getClave().trim().equals(antigua.trim()))) {
			throw new Exception("La clave no es correcta");
		}
		cuenta.setClave(nueva.trim());
		update(cuenta);
		enviarCorreoService.enviarCorreoCambioDeClave(cuenta.getCliente().getEmail(), cuenta.getCuenId(), cuenta.getClave());
	}

	@Override
	@Transactional(readOnly=true)
	public List<Cuenta> cuentasRegistradasPorCliente(Long idCliente) throws Exception {
		List<Cuenta> cuentas = new ArrayList<>();
		List<CuentaRegistrada> cuentasRegistradas = cuentaRegistradaService.cuentasRegistradasPorCliente(idCliente);
		for (CuentaRegistrada cuentaRegistrada : cuentasRegistradas) {
			Cuenta cuenta = cuentaRegistrada.getCuenta();
			cuentas.add(cuenta);
		}
		return cuentas;
	}

}
