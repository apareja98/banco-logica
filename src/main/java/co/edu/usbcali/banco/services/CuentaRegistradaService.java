package co.edu.usbcali.banco.services;

import java.util.List;

import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.dto.CuentaRegistradaDTO;


public interface CuentaRegistradaService  {
	
	public void save(CuentaRegistrada cuentaRegistrada) throws Exception;
	
	public void update(CuentaRegistrada cuentaRegistrada)throws Exception;
	
	public void delete(CuentaRegistrada cuentaRegistrada)throws Exception;
	
	public CuentaRegistrada findById(Long id)throws Exception;
	
	public List<CuentaRegistrada> findAll()throws Exception;

	public List<CuentaRegistradaDTO> findAllDTO()throws Exception;
	
	public Boolean esCuentaRegistrada(String cuenId, Long clieId)throws Exception;
	
	public void registrarCuenta(Long clieId, Long tiDocId, Long id, String cuenId)throws Exception;
	
	public List<CuentaRegistrada> cuentasRegistradasPorCliente(Long clieId)throws Exception;
	

}
