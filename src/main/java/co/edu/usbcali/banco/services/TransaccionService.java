package co.edu.usbcali.banco.services;

import java.util.List;

import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.dto.TransaccionDTO;



public interface TransaccionService {

	public Transaccion save(Transaccion transaccion) throws Exception;

	public void update(Transaccion transaccion) throws Exception;

	public void delete(Transaccion transaccion) throws Exception;

	public Transaccion findById(Long id) throws Exception;

	public List<Transaccion> findAll() throws Exception;
	
	public List<TransaccionDTO> findAllDTO() throws Exception;
	
	public List<Transaccion> transaccionesPorCliente(Long idCliente)throws Exception;
	
	public List<Transaccion> transaccionesPorCuenta(String cuenId) throws Exception;

}
