package co.edu.usbcali.banco.services;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.repository.UsuarioRepository;

@Service
@Scope("singleton")
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private Validator validator;

	public void validarUsuario(Usuario usuario) throws Exception {
		try {
			Set<ConstraintViolation<Usuario>> constraintViolations = validator.validate(usuario);

			if (constraintViolations.size() > 0) {
				StringBuilder strMessage = new StringBuilder();

				for (ConstraintViolation<Usuario> constraintViolation : constraintViolations) {
					strMessage.append(constraintViolation.getPropertyPath().toString());
					strMessage.append(" - ");
					strMessage.append(constraintViolation.getMessage());
					strMessage.append(". \n");
				}

				throw new Exception(strMessage.toString());
			}
		} catch (Exception e) {
			throw e;
		}
	}


	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(Usuario usuario) throws Exception {
		if(usuario==null)
			throw new Exception("El usuario es nulo y no debe serlo");
		validarUsuario(usuario);
		if(findById(usuario.getUsuUsuario())!=null)
			throw new Exception("Ya hay un usuario registrado con este nombre de usuario");
		usuarioRepository.save(usuario);

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void update(Usuario usuario) throws Exception {
		// TODO Auto-generated method stub
		if(usuario==null)
			throw new Exception("El usuario es nulo y no debe serlo");
		validarUsuario(usuario);
		usuarioRepository.update(usuario);

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(Usuario usuario) throws Exception {
		if(usuario==null)
			throw new Exception("El usuario es nulo y no debe serlo");
		usuario=findById(usuario.getUsuUsuario());
		if(usuario.getTransaccions()!=null && usuario.getTransaccions().size()>0)
			throw new Exception("El usuario tiene transacciones asociadas, no se puede borrar");
		usuarioRepository.delete(usuario);
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly=true)
	public Usuario findById(String id) {
		// TODO Auto-generated method stub
		return usuarioRepository.findById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Usuario> findAll() throws Exception {
		// TODO Auto-generated method stub
		return usuarioRepository.findAll();
	}


	@Override
	@Transactional(readOnly=true)
	public Boolean loginUsuario(String usuUsuario, String clave) throws Exception{
		Usuario usuario = this.findById(usuUsuario);
		if(usuario == null)return false;
		if(usuario.getActivo().equalsIgnoreCase("N")==true)return false;
		if(usuario.getClave().trim().equals(clave.trim()))return true;
		return false;
	}


	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void inactivarUsuario(String usuUsuario) throws Exception {
		Usuario usuario = this.findById(usuUsuario);
		if(usuario !=null) {
			usuario.setActivo("N");
			update(usuario);
		}
		
	}
	

}
