package co.edu.usbcali.banco.services;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.repository.ClienteRepository;

@Service
@Scope("singleton")
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private CuentaService cuentaService;

	@Autowired
	private Validator validator;

	public void validarClientes(Cliente cliente) throws Exception {
		try {
			Set<ConstraintViolation<Cliente>> constraintViolations = validator.validate(cliente);

			if (constraintViolations.size() > 0) {
				StringBuilder strMessage = new StringBuilder();

				for (ConstraintViolation<Cliente> constraintViolation : constraintViolations) {
					strMessage.append(constraintViolation.getPropertyPath().toString());
					strMessage.append(" - ");
					strMessage.append(constraintViolation.getMessage());
					strMessage.append(". \n");
				}

				throw new Exception(strMessage.toString());
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(Cliente cliente) throws Exception {

		if (cliente == null)
			throw new Exception("El cliente no debe ser nulo");

		validarClientes(cliente);

		if (findById(cliente.getClieId()) != null)
			throw new Exception("El cliente ya existe, no se puede crear");
		clienteRepository.save(cliente);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void update(Cliente cliente) throws Exception {

		if (cliente == null)
			throw new Exception("El cliente es nulo y no debe ser nulo");

		validarClientes(cliente);
		clienteRepository.update(cliente);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(Cliente cliente) throws Exception {

		if (cliente == null)
			throw new Exception("El cliente es nulo y no debe ser nulo");
		// se consulta de nuevo el cliente para que lo cargue
		cliente = findById(cliente.getClieId());
		if (cliente.getCuentaRegistradas() != null && cliente.getCuentaRegistradas().size() > 0)
			throw new Exception("El cliente no se puede borrar porque tiene cuentas registradas");

		if (cliente.getCuentas() != null && cliente.getCuentas().size() > 0)
			throw new Exception("El cliente no se puede borrar porque tiene cuentas");

		clienteRepository.delete(cliente);
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente findById(Long id) throws Exception {
		return clienteRepository.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAll() throws Exception {
		return clienteRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente loginCliente(String cuenId, Long id, String clave) throws Exception {
		Cuenta cuenta = cuentaService.findById(cuenId);
		if ((cuenta == null)  || (cuenta.getCliente().getClieId() != id)
				|| (!(cuenta.getClave().trim().equals(clave.trim()))))
			throw new Exception("Credenciales erroneas");
		if((cuenta.getActiva().equalsIgnoreCase("N")))throw new Exception("La cuenta está inactiva, para activarla debe consignar como minimo $200.000");
		return cuenta.getCliente();

	}

	@Override
	@Transactional(readOnly=true)
	public Cliente consultarCliente(String cuenId, Long clieId, Long tiDocId) throws Exception {
		Cuenta cuenta = cuentaService.findById(cuenId);
		if(cuenta==null)throw new Exception("Los datos no son correctos, revisalos");
		Cliente cliente = clienteRepository.findById(clieId);
		if(cliente==null)throw new Exception("Los datos no son correctos, revisalos");
		if(!(cliente.getTipoDocumento().getTdocId()==tiDocId))throw new Exception("Los datos no son correctos, revisalos");
		
		return cliente;
	}

}
