package co.edu.usbcali.banco.services;

import java.util.List;

import co.edu.usbcali.banco.domain.Usuario;





public interface UsuarioService {

	public void save(Usuario usuario) throws Exception;

	public void update(Usuario usuario) throws Exception;

	public void delete(Usuario usuario) throws Exception;

	public Usuario findById(String id) ;

	public List<Usuario> findAll() throws Exception;
	
	public Boolean loginUsuario(String usuUsuario, String clave)throws Exception;
	
	public void inactivarUsuario(String usuUsuario)throws Exception;

}
