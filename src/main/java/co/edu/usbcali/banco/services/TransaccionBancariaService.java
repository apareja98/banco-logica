package co.edu.usbcali.banco.services;

import java.math.BigDecimal;

import co.edu.usbcali.banco.domain.Cuenta;

public interface TransaccionBancariaService {
	
	public Cuenta consignar(String cuenId, String usuUsuario, BigDecimal valor)throws Exception;
		
	public Cuenta retirar(String cuenId, String usuUsuario, BigDecimal valor)throws Exception;
	

	public Cuenta traslado(String cuenIdOrigen,String cuenIdDestino, String usuUsuario, BigDecimal valor)throws Exception;
	
	public Cuenta retirarATM(String cuenId,String clave, String usuUsuario, BigDecimal valor) throws Exception;

	public Cuenta consultaSaldo(String cuenId, String clave)throws Exception;

}
