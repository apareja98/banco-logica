package co.edu.usbcali.banco.services;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.usbcali.banco.domain.TipoTransaccion;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
@Service
@Scope("singleton")
public class EnviarCorreoServiceImpl implements EnviarCorreoService {
	
	@Autowired
	private JavaMailSender mailSender;
	
	
	@Override
	@Async
	public void enviarCorreoCreacionCuenta(String correo, String cuenId, String clave) throws Exception {
		// TODO Auto-generated method stubMimeMessage message = mailSender.createMimeMessage();
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(correo);
		helper.setSubject("Se ha creado una cuenta");
		helper.setText("<!DOCTYPE html>\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<title>A Responsive Email Template</title>\n" + 
				"<meta charset=\"utf-8\">\n" + 
				"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" + 
				"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" + 
				"<style type=\"text/css\">\n" + 
				"    /* CLIENT-SPECIFIC STYLES */\n" + 
				"    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */\n" + 
				"    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */\n" + 
				"    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */\n" + 
				"\n" + 
				"    /* RESET STYLES */\n" + 
				"    img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}\n" + 
				"    table{border-collapse: collapse !important;}\n" + 
				"    body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}\n" + 
				"\n" + 
				"    /* iOS BLUE LINKS */\n" + 
				"    a[x-apple-data-detectors] {\n" + 
				"        color: inherit !important;\n" + 
				"        text-decoration: none !important;\n" + 
				"        font-size: inherit !important;\n" + 
				"        font-family: inherit !important;\n" + 
				"        font-weight: inherit !important;\n" + 
				"        line-height: inherit !important;\n" + 
				"    }\n" + 
				"\n" + 
				"    /* MOBILE STYLES */\n" + 
				"    @media screen and (max-width: 525px) {\n" + 
				"\n" + 
				"        /* ALLOWS FOR FLUID TABLES */\n" + 
				"        .wrapper {\n" + 
				"          width: 100% !important;\n" + 
				"            max-width: 100% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* ADJUSTS LAYOUT OF LOGO IMAGE */\n" + 
				"        .logo img {\n" + 
				"          margin: 0 auto !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\n" + 
				"        .mobile-hide {\n" + 
				"          display: none !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .img-max {\n" + 
				"          max-width: 100% !important;\n" + 
				"          width: 100% !important;\n" + 
				"          height: auto !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* FULL-WIDTH TABLES */\n" + 
				"        .responsive-table {\n" + 
				"          width: 100% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\n" + 
				"        .padding {\n" + 
				"          padding: 10px 5% 15px 5% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .padding-meta {\n" + 
				"          padding: 30px 5% 0px 5% !important;\n" + 
				"          text-align: center;\n" + 
				"        }\n" + 
				"\n" + 
				"        .padding-copy {\n" + 
				"             padding: 10px 5% 10px 5% !important;\n" + 
				"          text-align: center;\n" + 
				"        }\n" + 
				"\n" + 
				"        .no-padding {\n" + 
				"          padding: 0 !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .section-padding {\n" + 
				"          padding: 50px 15px 50px 15px !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* ADJUST BUTTONS ON MOBILE */\n" + 
				"        .mobile-button-container {\n" + 
				"            margin: 0 auto;\n" + 
				"            width: 100% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .mobile-button {\n" + 
				"            padding: 15px !important;\n" + 
				"            border: 0 !important;\n" + 
				"            font-size: 16px !important;\n" + 
				"            display: block !important;\n" + 
				"        }\n" + 
				"\n" + 
				"    }\n" + 
				"\n" + 
				"    /* ANDROID CENTER FIX */\n" + 
				"    div[style*=\"margin: 16px 0;\"] { margin: 0 !important; }\n" + 
				"</style>\n" + 
				"</head>\n" + 
				"<body style=\"margin: 0 !important; padding: 0 !important;\">\n" + 
				"\n" + 
				"<!-- HIDDEN PREHEADER TEXT -->\n" + 
				"<div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\n" + 
				"    Se ha creado una cuenta a tu nombre en Banco Web\n" + 
				"</div>\n" + 
				"\n" + 
				"<!-- HEADER -->\n" + 
				"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" + 
				"    <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\n" + 
				"                <tr>\n" + 
				"                    <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\n" + 
		                   
				"                            <img alt=\"Logo\" src=\"https://image.ibb.co/jc2JTV/logo-banco.png\" width=\"60\" height=\"60\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\n" + 
			
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"    <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px;\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <!-- COPY -->\n" + 
				"                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
				"                            <tr>\n" + 
				"                                <td align=\"center\" style=\"font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;\" class=\"padding-copy\">Se ha creado una nueva cuenta</td>\n" + 
				"                            </tr>\n" + 
				"                            <tr>\n" + 
				"                                <td align=\"left\" style=\"padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding-copy\">Se acaba de crear una nueva cuenta a tu nombre en Banco Web, los datos de tu cuenta son:</td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"\n" + 
				"    <tr>\n" + 
				"\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px;\" class=\"padding\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td style=\"padding: 10px 0 0 0; border-top: 0px dashed #aaaaaa;\">\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"left\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">Id de la cuenta:</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"right\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"right\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">" + cuenId + "</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" style=\"padding: 0;\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"left\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">Clave de la cuenta:</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"right\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"right\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">"+ clave + "</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" style=\"padding: 0;\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        \n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"right\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                   \n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"                <tr>\n" + 
				"                    <td style=\"padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 0px dashed #aaaaaa;\">\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                     </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"    <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px;\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
				"                            <tr>\n" + 
				"                                <td>\n" + 
				"                                    <!-- COPY -->\n" + 
				"                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
				"                                   </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"     <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 20px 0px;\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <!-- UNSUBSCRIBE COPY -->\n" + 
				"            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\n" + 
				"                        La umbría, Universidad San Buenaventura Cali\n" + 
				"                        <br>\n" + 
				"                        <a style=\"color: #666666; text-decoration: none;\">Alejandro Pareja Londoño</a>\n" + 
				"                        <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\n" + 
				"                        <a style=\"color: #666666; text-decoration: none;\">Aplicaciones Empresariales</a>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"</table>\n" + 
				"\n" + 
				"</body>\n" + 
				"</html>" ,true);
		this.mailSender.send(message);
		

	}
	
	@Override
	@Async
	public void enviarCorreoTransaccion(String correo, BigDecimal valor, String cuen_id, TipoTransaccion tipoTransaccion) throws Exception {
		// TODO Auto-generated method stubMimeMessage message = mailSender.createMimeMessage();
		MimeMessage message = mailSender.createMimeMessage();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(correo);
		helper.setSubject("Se realizó " + tipoTransaccion.getNombre().toLowerCase() + " en una de tus cuentas");
		helper.setText("<!DOCTYPE html>\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<title>A Responsive Email Template</title>\n" + 
				"<!--\n" + 
				"\n" + 
				"    An email present from your friends at Litmus (@litmusapp)\n" + 
				"\n" + 
				"    Email is surprisingly hard. While this has been thoroughly tested, your mileage may vary.\n" + 
				"    It's highly recommended that you test using a service like Litmus (http://litmus.com) and your own devices.\n" + 
				"\n" + 
				"    Enjoy!\n" + 
				"\n" + 
				" -->\n" + 
				"<meta charset=\"utf-8\">\n" + 
				"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" + 
				"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" + 
				"<style type=\"text/css\">\n" + 
				"    /* CLIENT-SPECIFIC STYLES */\n" + 
				"    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */\n" + 
				"    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */\n" + 
				"    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */\n" + 
				"\n" + 
				"    /* RESET STYLES */\n" + 
				"    img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}\n" + 
				"    table{border-collapse: collapse !important;}\n" + 
				"    body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}\n" + 
				"\n" + 
				"    /* iOS BLUE LINKS */\n" + 
				"    a[x-apple-data-detectors] {\n" + 
				"        color: inherit !important;\n" + 
				"        text-decoration: none !important;\n" + 
				"        font-size: inherit !important;\n" + 
				"        font-family: inherit !important;\n" + 
				"        font-weight: inherit !important;\n" + 
				"        line-height: inherit !important;\n" + 
				"    }\n" + 
				"\n" + 
				"    /* MOBILE STYLES */\n" + 
				"    @media screen and (max-width: 525px) {\n" + 
				"\n" + 
				"        /* ALLOWS FOR FLUID TABLES */\n" + 
				"        .wrapper {\n" + 
				"          width: 100% !important;\n" + 
				"            max-width: 100% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* ADJUSTS LAYOUT OF LOGO IMAGE */\n" + 
				"        .logo img {\n" + 
				"          margin: 0 auto !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\n" + 
				"        .mobile-hide {\n" + 
				"          display: none !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .img-max {\n" + 
				"          max-width: 100% !important;\n" + 
				"          width: 100% !important;\n" + 
				"          height: auto !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* FULL-WIDTH TABLES */\n" + 
				"        .responsive-table {\n" + 
				"          width: 100% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\n" + 
				"        .padding {\n" + 
				"          padding: 10px 5% 15px 5% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .padding-meta {\n" + 
				"          padding: 30px 5% 0px 5% !important;\n" + 
				"          text-align: center;\n" + 
				"        }\n" + 
				"\n" + 
				"        .padding-copy {\n" + 
				"             padding: 10px 5% 10px 5% !important;\n" + 
				"          text-align: center;\n" + 
				"        }\n" + 
				"\n" + 
				"        .no-padding {\n" + 
				"          padding: 0 !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .section-padding {\n" + 
				"          padding: 50px 15px 50px 15px !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* ADJUST BUTTONS ON MOBILE */\n" + 
				"        .mobile-button-container {\n" + 
				"            margin: 0 auto;\n" + 
				"            width: 100% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .mobile-button {\n" + 
				"            padding: 15px !important;\n" + 
				"            border: 0 !important;\n" + 
				"            font-size: 16px !important;\n" + 
				"            display: block !important;\n" + 
				"        }\n" + 
				"\n" + 
				"    }\n" + 
				"\n" + 
				"    /* ANDROID CENTER FIX */\n" + 
				"    div[style*=\"margin: 16px 0;\"] { margin: 0 !important; }\n" + 
				"</style>\n" + 
				"</head>\n" + 
				"<body style=\"margin: 0 !important; padding: 0 !important;\">\n" + 
				"\n" + 
				"<!-- HIDDEN PREHEADER TEXT -->\n" + 
				"<div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\n" + 
				"    Se ha creado una cuenta a tu nombre en Banco Web\n" + 
				"</div>\n" + 
				"\n" + 
				"<!-- HEADER -->\n" + 
				"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" + 
				"    <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\n" + 
				"                <tr>\n" + 
				"                    <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\n" + 
				
				"                            <img alt=\"Logo\" src=\"https://image.ibb.co/jc2JTV/logo-banco.png\" width=\"60\" height=\"60\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\n" + 
				 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"    <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px;\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <!-- COPY -->\n" + 
				"                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
				"                            <tr>\n" + 
				"                                <td align=\"center\" style=\"font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;\" class=\"padding-copy\">Se realizó Una transacción en una de tus cuentas</td>\n" + 
				"                            </tr>\n" + 
				"                            <tr>\n" + 
				"                                <td align=\"left\" style=\"padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding-copy\"> Se realizó " +tipoTransaccion.getNombre().toLowerCase() + " en la cuenta " + cuen_id +  " el " + new Date().toLocaleString() + " a las " + sdf.format(new Date()).toString() + "</td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"\n" + 
				"    <tr>\n" + 
				"\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px;\" class=\"padding\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td style=\"padding: 10px 0 0 0; border-top: 0px dashed #aaaaaa;\">\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"left\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">Tipo de transacción: </td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"right\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"right\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">" + tipoTransaccion.getNombre() + "</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" style=\"padding: 0;\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"left\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">Valor de la transacción:</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"right\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"right\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">"+ valor.toString() + "</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" style=\"padding: 0;\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        \n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"right\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                   \n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"                <tr>\n" + 
				"                    <td style=\"padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 0px dashed #aaaaaa;\">\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                     </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"    <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px;\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
				"                            <tr>\n" + 
				"                                <td>\n" + 
				"                                    <!-- COPY -->\n" + 
				"                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
				"                                   </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"     <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 20px 0px;\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <!-- UNSUBSCRIBE COPY -->\n" + 
				"            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\n" + 
				"                        La umbría, Universidad San Buenaventura Cali\n" + 
				"                        <br>\n" + 
				"                        <a style=\"color: #666666; text-decoration: none;\">Alejandro Pareja Londoño</a>\n" + 
				"                        <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\n" + 
				"                        <a style=\"color: #666666; text-decoration: none;\">Aplicaciones Empresariales</a>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"</table>\n" + 
				"\n" + 
				"</body>\n" + 
				"</html>" ,true);
		this.mailSender.send(message);
		

	}

	@Override
	@Async
	public void enviarCorreoCambioDeClave(String correo,String cuenId, String clave) throws Exception {
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(correo);
		helper.setSubject("Cambio de contraseña");
		helper.setText("<!DOCTYPE html>\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<title>Cambio de contraseña</title>\n" + 
				"<meta charset=\"utf-8\">\n" + 
				"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" + 
				"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" + 
				"<style type=\"text/css\">\n" + 
				"    /* CLIENT-SPECIFIC STYLES */\n" + 
				"    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */\n" + 
				"    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */\n" + 
				"    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */\n" + 
				"\n" + 
				"    /* RESET STYLES */\n" + 
				"    img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}\n" + 
				"    table{border-collapse: collapse !important;}\n" + 
				"    body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}\n" + 
				"\n" + 
				"    /* iOS BLUE LINKS */\n" + 
				"    a[x-apple-data-detectors] {\n" + 
				"        color: inherit !important;\n" + 
				"        text-decoration: none !important;\n" + 
				"        font-size: inherit !important;\n" + 
				"        font-family: inherit !important;\n" + 
				"        font-weight: inherit !important;\n" + 
				"        line-height: inherit !important;\n" + 
				"    }\n" + 
				"\n" + 
				"    /* MOBILE STYLES */\n" + 
				"    @media screen and (max-width: 525px) {\n" + 
				"\n" + 
				"        /* ALLOWS FOR FLUID TABLES */\n" + 
				"        .wrapper {\n" + 
				"          width: 100% !important;\n" + 
				"            max-width: 100% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* ADJUSTS LAYOUT OF LOGO IMAGE */\n" + 
				"        .logo img {\n" + 
				"          margin: 0 auto !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\n" + 
				"        .mobile-hide {\n" + 
				"          display: none !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .img-max {\n" + 
				"          max-width: 100% !important;\n" + 
				"          width: 100% !important;\n" + 
				"          height: auto !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* FULL-WIDTH TABLES */\n" + 
				"        .responsive-table {\n" + 
				"          width: 100% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\n" + 
				"        .padding {\n" + 
				"          padding: 10px 5% 15px 5% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .padding-meta {\n" + 
				"          padding: 30px 5% 0px 5% !important;\n" + 
				"          text-align: center;\n" + 
				"        }\n" + 
				"\n" + 
				"        .padding-copy {\n" + 
				"             padding: 10px 5% 10px 5% !important;\n" + 
				"          text-align: center;\n" + 
				"        }\n" + 
				"\n" + 
				"        .no-padding {\n" + 
				"          padding: 0 !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .section-padding {\n" + 
				"          padding: 50px 15px 50px 15px !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        /* ADJUST BUTTONS ON MOBILE */\n" + 
				"        .mobile-button-container {\n" + 
				"            margin: 0 auto;\n" + 
				"            width: 100% !important;\n" + 
				"        }\n" + 
				"\n" + 
				"        .mobile-button {\n" + 
				"            padding: 15px !important;\n" + 
				"            border: 0 !important;\n" + 
				"            font-size: 16px !important;\n" + 
				"            display: block !important;\n" + 
				"        }\n" + 
				"\n" + 
				"    }\n" + 
				"\n" + 
				"    /* ANDROID CENTER FIX */\n" + 
				"    div[style*=\"margin: 16px 0;\"] { margin: 0 !important; }\n" + 
				"</style>\n" + 
				"</head>\n" + 
				"<body style=\"margin: 0 !important; padding: 0 !important;\">\n" + 
				"\n" + 
				"<!-- HIDDEN PREHEADER TEXT -->\n" + 
				"<div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\n" + 
				"    Se ha creado una cuenta a tu nombre en Banco Web\n" + 
				"</div>\n" + 
				"\n" + 
				"<!-- HEADER -->\n" + 
				"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" + 
				"    <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\n" + 
				"                <tr>\n" + 
				"                    <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\n" + 
		                   
				"                            <img alt=\"Logo\" src=\"https://image.ibb.co/jc2JTV/logo-banco.png\" width=\"60\" height=\"60\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\n" + 
			
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"    <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px;\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <!-- COPY -->\n" + 
				"                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
				"                            <tr>\n" + 
				"                                <td align=\"center\" style=\"font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;\" class=\"padding-copy\">Cambio de clave</td>\n" + 
				"                            </tr>\n" + 
				"                            <tr>\n" + 
				"                                <td align=\"left\" style=\"padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding-copy\">Se acaba de cambiar la clave en una de tus cuentas, los datos de dicha cuenta son:</td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"\n" + 
				"    <tr>\n" + 
				"\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px;\" class=\"padding\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td style=\"padding: 10px 0 0 0; border-top: 0px dashed #aaaaaa;\">\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"left\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">Id de la cuenta:</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"right\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"right\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">" + cuenId + "</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" style=\"padding: 0;\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"left\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">Clave de la cuenta:</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"right\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        <td align=\"right\" style=\"font-family: Arial, sans-serif; color: #333333; font-size: 16px;\">"+ clave + "</td>\n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" style=\"padding: 0;\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                        \n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"right\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                    <tr>\n" + 
				"                                                   \n" + 
				"                                                    </tr>\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"                <tr>\n" + 
				"                    <td style=\"padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 0px dashed #aaaaaa;\">\n" + 
				"                        <!-- TWO COLUMNS -->\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                            <tr>\n" + 
				"                                <td valign=\"top\" class=\"mobile-wrapper\">\n" + 
				"                                    <!-- LEFT COLUMN -->\n" + 
				"                                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"47%\" style=\"width: 47%;\" align=\"left\">\n" + 
				"                                        <tr>\n" + 
				"                                            <td style=\"padding: 0 0 10px 0;\">\n" + 
				"                                                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n" + 
				"                                                </table>\n" + 
				"                                            </td>\n" + 
				"                                        </tr>\n" + 
				"                                    </table>\n" + 
				"                                    <!-- RIGHT COLUMN -->\n" + 
				"                                     </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"    <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px;\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td>\n" + 
				"                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
				"                            <tr>\n" + 
				"                                <td>\n" + 
				"                                    <!-- COPY -->\n" + 
				"                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
				"                                   </table>\n" + 
				"                                </td>\n" + 
				"                            </tr>\n" + 
				"                        </table>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"     <tr>\n" + 
				"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 20px 0px;\">\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\n" + 
				"            <tr>\n" + 
				"            <td align=\"center\" valign=\"top\" width=\"500\">\n" + 
				"            <![endif]-->\n" + 
				"            <!-- UNSUBSCRIBE COPY -->\n" + 
				"            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\n" + 
				"                <tr>\n" + 
				"                    <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\n" + 
				"                        La umbría, Universidad San Buenaventura Cali\n" + 
				"                        <br>\n" + 
				"                        <a style=\"color: #666666; text-decoration: none;\">Alejandro Pareja Londoño</a>\n" + 
				"                        <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\n" + 
				"                        <a style=\"color: #666666; text-decoration: none;\">Aplicaciones Empresariales</a>\n" + 
				"                    </td>\n" + 
				"                </tr>\n" + 
				"            </table>\n" + 
				"            <!--[if (gte mso 9)|(IE)]>\n" + 
				"            </td>\n" + 
				"            </tr>\n" + 
				"            </table>\n" + 
				"            <![endif]-->\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"</table>\n" + 
				"\n" + 
				"</body>\n" + 
				"</html>" ,true);
		this.mailSender.send(message);
		


		
	}


}
