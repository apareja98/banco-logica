package co.edu.usbcali.banco.services;

import java.util.List;

import co.edu.usbcali.banco.domain.TipoTransaccion;



public interface TipoTransaccionService {
	
	public void save(TipoTransaccion tipoTransaccion) throws Exception;
	
	public void update(TipoTransaccion tipoTransaccion) throws Exception;
	
	public void delete(TipoTransaccion tipoTransaccion) throws Exception;
	
	public TipoTransaccion findById(Long id) throws Exception;
	
	public List<TipoTransaccion> findAll() throws Exception;

}
