package co.edu.usbcali.banco.services;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import co.edu.usbcali.banco.domain.TipoTransaccion;
import co.edu.usbcali.banco.repository.TipoTransaccionRepository;

@Service
@Scope("singleton")
public class TipoTransaccionServiceImpl implements TipoTransaccionService {

	@Autowired
	private TipoTransaccionRepository tipoTransaccionRepository;

	@Autowired
	private Validator validator;

	public void validarTipoTransaccion(TipoTransaccion tipoTransaccion) throws Exception {
		try {
			Set<ConstraintViolation<TipoTransaccion>> constraintViolations = validator.validate(tipoTransaccion);

			if (constraintViolations.size() > 0) {
				StringBuilder strMessage = new StringBuilder();

				for (ConstraintViolation<TipoTransaccion> constraintViolation : constraintViolations) {
					strMessage.append(constraintViolation.getPropertyPath().toString());
					strMessage.append(" - ");
					strMessage.append(constraintViolation.getMessage());
					strMessage.append(". \n");
				}

				throw new Exception(strMessage.toString());
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(TipoTransaccion tipoTransaccion) throws Exception {
		if (tipoTransaccion == null)
			throw new Exception("El tipo de transacción es nulo y no debe serlo");
		validarTipoTransaccion(tipoTransaccion);
		if (findById(tipoTransaccion.getTitrId()) != null)
			throw new Exception("Ya hay un tipo de transacción con este id");
		tipoTransaccionRepository.save(tipoTransaccion);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void update(TipoTransaccion tipoTransaccion) throws Exception {
		if (tipoTransaccion == null)
			throw new Exception("El tipo de transacción es nulo y no debe serlo");
		validarTipoTransaccion(tipoTransaccion);
		tipoTransaccionRepository.update(tipoTransaccion);

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(TipoTransaccion tipoTransaccion) throws Exception {
		if (tipoTransaccion == null)
			throw new Exception("El tipo de transacción es nulo y no debe serlo");
		tipoTransaccion = findById(tipoTransaccion.getTitrId());
		if (tipoTransaccion.getTransaccions() != null && tipoTransaccion.getTransaccions().size() > 0)
			throw new Exception(
					"El tipo de transacción está siendo utilizado por algunas transacciones, no se puede borrar");
		tipoTransaccionRepository.delete(tipoTransaccion);
	}

	@Override
	@Transactional(readOnly=true)
	public TipoTransaccion findById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return tipoTransaccionRepository.findById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<TipoTransaccion> findAll() throws Exception {
		// TODO Auto-generated method stub
		return tipoTransaccionRepository.findAll();
	}

}
