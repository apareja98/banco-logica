package co.edu.usbcali.banco.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.usbcali.banco.domain.Cuenta;
@Repository
@Scope("singleton")
public class CuentaRepositoryImpl implements CuentaRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void save(Cuenta cuenta) {
		
		entityManager.persist(cuenta);
		

	}

	@Override
	public void update(Cuenta cuenta) {
		entityManager.merge(cuenta);
	}

	@Override
	public void delete(Cuenta cuenta) {
		entityManager.remove(cuenta);

	}

	@Override
	public Cuenta findById(String id) {
		// TODO Auto-generated method stub
		return entityManager.find(Cuenta.class, id);
	}

	@Override
	public List<Cuenta> findAll() {
		// TODO Auto-generated method stub
		return	entityManager.createNamedQuery("Cuenta.findAll").getResultList();
	}

	@Override
	public List<Cuenta> cuentasPorCliente(Long idCliente) {
		String hql = "SELECT cuen FROM Cuenta cuen WHERE cuen.cliente.clieId =" + idCliente;
		return entityManager.createQuery(hql).getResultList();
	}

}
