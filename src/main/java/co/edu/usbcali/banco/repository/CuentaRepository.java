package co.edu.usbcali.banco.repository;

import java.util.List;


import co.edu.usbcali.banco.domain.Cuenta;

public interface CuentaRepository {
	
	public void save(Cuenta cuenta);
	
	public void update(Cuenta cuenta);
	
	public void delete(Cuenta cuenta);
	
	public Cuenta findById(String id);
	
	public List<Cuenta> findAll();
	
	public List<Cuenta> cuentasPorCliente(Long idCliente);

}
