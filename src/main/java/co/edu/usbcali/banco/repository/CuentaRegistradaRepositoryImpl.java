package co.edu.usbcali.banco.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.dto.CuentaRegistradaDTO;

@Repository
@Scope("singleton")
public class CuentaRegistradaRepositoryImpl implements CuentaRegistradaRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void save(CuentaRegistrada cuentaRegistrada) {
		entityManager.persist(cuentaRegistrada);

	}

	@Override
	public void update(CuentaRegistrada cuentaRegistrada) {
		entityManager.merge(cuentaRegistrada);

	}

	@Override
	public void delete(CuentaRegistrada cuentaRegistrada) {
		entityManager.remove(cuentaRegistrada);
	}

	@Override
	public CuentaRegistrada findById(Long id) {
		// TODO Auto-generated method stub
		return entityManager.find(CuentaRegistrada.class, id);
	}

	@Override
	public List<CuentaRegistrada> findAll() {
		// TODO Auto-generated method stub
		return entityManager.createNamedQuery("CuentaRegistrada.findAll").getResultList();
	}

	@Override
	public List<CuentaRegistradaDTO> findAllDTO() {
		// TODO Auto-generated method stub
		return entityManager.createNamedQuery("CuentaRegistrada.findAllDTO").getResultList();
	}

	@Override
	public List<CuentaRegistrada> findCuenta(String cuen_id) {
		String hql = "SELECT cure FROM CuentaRegistrada cure WHERE cure.cuenta.cuenId ='"+cuen_id + "'";
		return entityManager.createQuery(hql).getResultList();
	}

	@Override
	public List<CuentaRegistrada> cuentaRegistradaCliente(String cuen_id, Long clieId) {
		String hql = "SELECT cure FROM CuentaRegistrada cure WHERE cure.cuenta.cuenId ='"+cuen_id + "' AND cure.cliente.clieId =" + clieId;
		return entityManager.createQuery(hql).getResultList();
	}

	@Override
	public List<CuentaRegistrada> cuentasRegistradasPorCliente(Long clieId) {
		String hql ="SELECT cure FROM CuentaRegistrada cure WHERE cure.cliente.clieId=" + clieId;
		return entityManager.createQuery(hql).getResultList();
	}
	
	

}
