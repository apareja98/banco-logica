package co.edu.usbcali.banco.repository;

import java.util.List;

import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.dto.CuentaRegistradaDTO;


public interface CuentaRegistradaRepository {
	
	public void save(CuentaRegistrada cuentaRegistrada);
	
	public void update(CuentaRegistrada cuentaRegistrada);
	
	public void delete(CuentaRegistrada cuentaRegistrada);
	
	public CuentaRegistrada findById(Long id);
	
	public List<CuentaRegistrada> findAll();
	
	public List<CuentaRegistradaDTO> findAllDTO();

	public List<CuentaRegistrada> findCuenta(String cuen_id);
	
	public List<CuentaRegistrada> cuentaRegistradaCliente(String cuen_id, Long clieId);
	
	public List<CuentaRegistrada> cuentasRegistradasPorCliente(Long clieId);
}
