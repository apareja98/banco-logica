package co.edu.usbcali.banco.repository;

import java.util.List;

import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.dto.TransaccionDTO;



public interface TransaccionRepository {

	public void save(Transaccion transaccion);

	public void update(Transaccion transaccion);

	public void delete(Transaccion transaccion);

	public Transaccion findById(Long id);

	public List<Transaccion> findAll();
	
	public List<TransaccionDTO> findAllDTO();
	
	public List<Transaccion> ultimasTransaccionesPorCliente(Long idCliente);
	
	public List<Transaccion>transaccionesPorCuenta(String cuenId);

}
