package co.edu.usbcali.banco.repository;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;


public class CuentaIdGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		Random ran = new Random();
		String a = String.format("%04d", ran.nextInt(10000));
		String b = String.format("%04d", ran.nextInt(10000));
		String c = String.format("%04d", ran.nextInt(10000));
		Connection connection = session.connection();
	        try {

	            PreparedStatement ps = connection
	                    .prepareStatement("SELECT nextval ('idcuenta') as nextval");

	            ResultSet rs = ps.executeQuery();
	            if (rs.next()) {
	                int id = rs.getInt("nextval");
	                String code = a + "-" + b + "-" + c + "-" + String.format("%04d", id);
	                return code;
	            }

	        } catch (SQLException e) {
	            throw new HibernateException(
	                    "Unable to generate Stock Code Sequence");
	        }
		return null;
	}

}
