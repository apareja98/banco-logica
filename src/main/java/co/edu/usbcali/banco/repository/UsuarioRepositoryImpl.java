package co.edu.usbcali.banco.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.usbcali.banco.domain.Usuario;
@Repository
@Scope("singleton")
public class UsuarioRepositoryImpl implements UsuarioRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void save(Usuario usuario) {
		entityManager.persist(usuario);

	}

	@Override
	public void update(Usuario usuario) {
		// TODO Auto-generated method stub
		entityManager.merge(usuario);
	}

	@Override
	public void delete(Usuario usuario) {
		// TODO Auto-generated method stub
		entityManager.remove(usuario);
	}

	@Override
	public Usuario findById(String id) {
		// TODO Auto-generated method stub
		return entityManager.find(Usuario.class, id);
	}

	@Override
	public List<Usuario> findAll() {
		// TODO Auto-generated method stub
		return entityManager.createNamedQuery("Usuario.findAll").getResultList();
	}

}
