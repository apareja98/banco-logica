package co.edu.usbcali.banco.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.dto.TransaccionDTO;

@Repository
@Scope("singleton")
public class TransaccionRepositoryImpl implements TransaccionRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(Transaccion transaccion) {
		entityManager.persist(transaccion);
	}

	@Override
	public void update(Transaccion transaccion) {
		entityManager.merge(transaccion);
	}

	@Override
	public void delete(Transaccion transaccion) {
		entityManager.remove(transaccion);

	}

	@Override
	public Transaccion findById(Long id) {
		return entityManager.find(Transaccion.class, id);
	}

	@Override
	public List<Transaccion> findAll() {
		return entityManager.createNamedQuery("Transaccion.findAll").getResultList();
	}

	@Override
	public List<TransaccionDTO> findAllDTO() {
		return entityManager.createNamedQuery("Transaccion.findDTO").getResultList();
	}

	@Override
	public List<Transaccion> ultimasTransaccionesPorCliente(Long idCliente) {
		String hql = "SELECT tran FROM Transaccion tran WHERE tran.cuenta.cliente.clieId=" + idCliente + " order by tran.fecha DESC";
		return entityManager.createQuery(hql).setMaxResults(5).getResultList();
	}

	@Override
	public List<Transaccion> transaccionesPorCuenta(String cuenId) {
		String hql = "SELECT tran FROM Transaccion tran WHERE tran.cuenta.cuenId = '" + cuenId + "' order by tran.fecha DESC";
		return entityManager.createQuery(hql).getResultList();
	}

}
