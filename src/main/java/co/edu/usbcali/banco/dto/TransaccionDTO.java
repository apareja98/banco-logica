package co.edu.usbcali.banco.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class TransaccionDTO {
	
	private Long tranId;
	private Date fecha;
	private BigDecimal valor;
	private String cuenId;
	private String nombreTipoTransaccion;
	private String usuUsuario;
	private String nombreTipoUsuario;
	
	public TransaccionDTO(Long tranId, Date fecha, BigDecimal valor, String cuenId, String nombreTipoTransaccion,
			String usuUsuario, String nombreTipoUsuario) {
		super();
		this.tranId = tranId;
		this.fecha = fecha;
		this.valor = valor;
		this.cuenId = cuenId;
		this.nombreTipoTransaccion = nombreTipoTransaccion;
		this.usuUsuario = usuUsuario;
		this.nombreTipoUsuario = nombreTipoUsuario;
	}

	public Long getTranId() {
		return tranId;
	}

	public void setTranId(Long tranId) {
		this.tranId = tranId;
	}

	public Date getFecha() {
		return fecha;	
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getCuenId() {
		return cuenId;
	}

	public void setCuenId(String cuenId) {
		this.cuenId = cuenId;
	}

	public String getNombreTipoTransaccion() {
		return nombreTipoTransaccion;
	}

	public void setNombreTipoTransaccion(String nombreTipoTransaccion) {
		this.nombreTipoTransaccion = nombreTipoTransaccion;
	}

	public String getUsuUsuario() {
		return usuUsuario;
	}

	public void setUsuUsuario(String usuUsuario) {
		this.usuUsuario = usuUsuario;
	}

	public String getNombreTipoUsuario() {
		return nombreTipoUsuario;
	}

	public void setNombreTipoUsuario(String nombreTipoUsuario) {
		this.nombreTipoUsuario = nombreTipoUsuario;
	}

	public TransaccionDTO() {
		
	}

	
	
	}
