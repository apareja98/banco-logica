package co.edu.usbcali.banco.dto;


public class CuentaRegistradaDTO {
	
	private Long cureId;
	private Long clieId;
	private String cuenId;
	
	public CuentaRegistradaDTO() {
		
	}

	public Long getCureId() {
		return cureId;
	}

	public void setCureId(Long cureId) {
		this.cureId = cureId;
	}

	public Long getClieId() {
		return clieId;
	}

	public void setClieId(Long clieId) {
		this.clieId = clieId;
	}

	public String getCuenId() {
		return cuenId;
	}

	public void setCuenId(String cuenId) {
		this.cuenId = cuenId;
	}

	public CuentaRegistradaDTO(Long cureId, Long clieId, String cuenId) {
		super();
		this.cureId = cureId;
		this.clieId = clieId;
		this.cuenId = cuenId;
	}
	
	
	}
