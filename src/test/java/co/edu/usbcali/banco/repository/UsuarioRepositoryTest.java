package co.edu.usbcali.banco.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.domain.Usuario;

@ExtendWith (SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")

@Rollback(false)
class UsuarioRepositoryTest {

	private final static Logger log = LoggerFactory.getLogger(UsuarioRepositoryTest.class);
	
	private final static String usuUsuario = "apareja1"; 
	
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private TipoUsuarioRepository tipoUsuarioRepository;
	
	
	
	@DisplayName("CrearUsuario")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	void aTest() {
		Usuario usuario = new Usuario();
		usuario.setUsuUsuario(usuUsuario);
		usuario.setNombre("Alejandro Pareja");
		usuario.setIdentificacion(new BigDecimal(1155486));
		usuario.setClave("111223sa");
		usuario.setActivo("S");
		TipoUsuario tipoUsuario = tipoUsuarioRepository.findById(2L);
		assertNotNull(tipoUsuario,"El tipo de usuario no existe");
		usuario.setTipoUsuario(tipoUsuario);
		usuarioRepository.save(usuario);
		
	}
	
	@Transactional(readOnly=true)
	@DisplayName ("Consultar usuario por id")
	@Test
	void bTest() {
		Usuario usuario = usuarioRepository.findById("apareja1");
		assertNotNull(usuario, "el usuario no existe");

	}
	@Transactional(readOnly=true)
	@DisplayName ("Consultar todos los usuarios")
	@Test
	void cTest() {
		List<Usuario> usuarios= usuarioRepository.findAll();
		usuarios.forEach(usuario ->{
			log.info(usuario.getNombre());
		});
	}
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@DisplayName ("Actualizar usuario")
	@Test
	void dTest() {
		Usuario usuario = usuarioRepository.findById("apareja1");
		assertNotNull(usuario, "el usuario no existe");
		usuario.setNombre("Carlos Gutierrez");
		usuarioRepository.update(usuario);
	}
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@DisplayName ("Borrar usuario")
	@Test
	void eTest() {
		Usuario usuario = usuarioRepository.findById("apareja1");
		assertNotNull(usuario, "el usuario no existe");
		usuario.setNombre("Carlos Gutierrez");
		usuarioRepository.delete(usuario);
	}
	
}
