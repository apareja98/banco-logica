package co.edu.usbcali.banco.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.TipoDocumento;


@ExtendWith (SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")

@Rollback(false)
class TipoDocumentoRepositoryTest {
	private final static Logger log = LoggerFactory.getLogger(TipoDocumentoRepositoryTest.class);
	
	private final static Long tdocId = 4L; 
	
	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;
	

	@DisplayName("Crear Tipo de Documento")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	
	void aTest() {
		TipoDocumento tipoDocumento = new TipoDocumento();
		tipoDocumento.setTdocId(tdocId);
		tipoDocumento.setNombre("PASAPORTE");
		tipoDocumento.setActivo("S");
		tipoDocumentoRepository.save(tipoDocumento);
	}
	//findById
	
	@DisplayName("Buscar tipoDocumento por id")
	@Test
	@Transactional(readOnly=true)
		void bTest() {
		TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(tdocId);
		assertNotNull(tipoDocumento,"El tipo de documento no existe");
		
	}
	
	@DisplayName("Buscar Todos los tipos de documento")
	@Test
	@Transactional(readOnly=true)
	void cTest() {
		List<TipoDocumento> tiposDocumento = tipoDocumentoRepository.findAll();
		tiposDocumento.forEach(tiDoc->{
			log.info(tiDoc.getNombre());
			log.info(tiDoc.getTdocId().toString());
		});
	}
	
	//update
	@DisplayName("Actualizar Tipo de documento")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	void dTest() {
		TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(tdocId);
		assertNotNull(tipoDocumento,"El tipo de documento no existe");
		tipoDocumento.setActivo("N");
		tipoDocumentoRepository.update(tipoDocumento);
		
	}
	//remove
	
	@DisplayName("Eliminar Tipo de documento")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)

	void eTest() {
		TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(tdocId);
		assertNotNull(tipoDocumento,"El tipo de documento no existe");
		tipoDocumentoRepository.delete(tipoDocumento);
		
		
	}
	
}
