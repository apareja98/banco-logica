package co.edu.usbcali.banco.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.TipoTransaccion;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")

@Rollback(false)
class TipoTransaccionRepositoryTest {

	private final static Logger log = LoggerFactory.getLogger(TipoTransaccionRepositoryTest.class);

	private final static Long titrId = 4L;

	@Autowired
	private TipoTransaccionRepository tipoTransaccionRepository;

	@DisplayName("Crear tipo de transaccion")
	@Test
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void aTest() {
		TipoTransaccion tipoTransaccion = new TipoTransaccion();
		tipoTransaccion.setTitrId(titrId);
		tipoTransaccion.setNombre("RECAUDO");
		tipoTransaccion.setActivo("S");
		tipoTransaccionRepository.save(tipoTransaccion);
	}

	@DisplayName("Consultar tipoTransaccion por Id")
	@Test
	@Transactional(readOnly = true)
	void bTest() {
		TipoTransaccion tiTransaccion = tipoTransaccionRepository.findById(titrId);
		assertNotNull(tiTransaccion, "El tipo de transaccion no existe");
	}

	@DisplayName("Listar todos los tipos de transaccion")
	@Test
	@Transactional(readOnly = true)
	void cTest() {

		List<TipoTransaccion> tiposTransaccion = tipoTransaccionRepository.findAll();
		tiposTransaccion.forEach(tipoUsuario -> {
			log.info(tipoUsuario.getNombre());
			log.info(tipoUsuario.getTitrId().toString());
		});

	}

	@DisplayName("Actualizar tipo de transaccion")
	@Test
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void dTest() {
		TipoTransaccion tipoTransaccion = tipoTransaccionRepository.findById(titrId);
		assertNotNull(tipoTransaccion, "El tipo de transacción no existe");
		tipoTransaccion.setActivo("N");
		tipoTransaccionRepository.update(tipoTransaccion);
	}

	@DisplayName("eliminar tipo de Transacción")
	@Test
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void eTest() {
		TipoTransaccion tipoTransaccion = tipoTransaccionRepository.findById(titrId);
		assertNotNull(tipoTransaccion, "El tipo de transacción no existe");
		tipoTransaccion.setActivo("N");
		tipoTransaccionRepository.delete(tipoTransaccion);
	}

}
