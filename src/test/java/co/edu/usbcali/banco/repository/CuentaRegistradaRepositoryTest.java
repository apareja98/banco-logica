package co.edu.usbcali.banco.repository;

import static org.junit.jupiter.api.Assertions.*;


import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.CuentaRegistrada;




@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(false)
class CuentaRegistradaRepositoryTest {

	private final static Logger log = LoggerFactory.getLogger(CuentaRegistradaRepositoryTest.class);
	
	@Autowired
	private CuentaRegistradaRepository cuentaRegistradaRepository;
	@Autowired
	private CuentaRepository cuentaRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	private final static Long cureId= 102L;
	private final static Long clieId=2L;
	private final static String cueId="3479-1726-5080-7363";
	
	
	@DisplayName("Crear cuenta registrada")
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Test
	void aTest() {
		CuentaRegistrada cuentaRe = new CuentaRegistrada();
		cuentaRe.setCureId(cureId);
		Cliente cliente = clienteRepository.findById(clieId);
		assertNotNull(cliente,"El cliente es nulo");
		Cuenta cuenta = cuentaRepository.findById(cueId);
		assertNotNull(cuenta,"la cuenta es nula");
		cuentaRe.setCliente(cliente);
		cuentaRe.setCuenta(cuenta);
		cuentaRegistradaRepository.save(cuentaRe);
	}
	
	@DisplayName("Buscar cuenta registrada")
	@Transactional(readOnly=true)
	@Test
	void bTest() {
		CuentaRegistrada cuentaRe = cuentaRegistradaRepository.findById(cureId);
		assertNotNull(cuentaRe,"La cuenta registrada no existe");
		
	}
	@DisplayName("Listar cuentas registradas")
	@Test
	@Transactional(readOnly=true)
	void cTest() {
		List<CuentaRegistrada> cuentasRegistradas = cuentaRegistradaRepository.findAll(); 
		cuentasRegistradas.forEach(cuentaRe->{
			log.info("--------------- Cuentas registradas---------");
			log.info(cuentaRe.getCureId().toString());
		});
	}
	
	@DisplayName("actualizar cuenta registrada")
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Test
	void dTest() {
		CuentaRegistrada cuentaRe = cuentaRegistradaRepository.findById(cureId);
		assertNotNull(cuentaRe,"la cuenta no existe");
		Cliente cliente = clienteRepository.findById(4L);
		assertNotNull(cliente,"El cliente no existe");
		cuentaRe.setCliente(cliente);
		cuentaRegistradaRepository.update(cuentaRe);
	}
	@DisplayName("Eliminar cuenta registrada")
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Test
	void eTest() {
		CuentaRegistrada cuentaRe = cuentaRegistradaRepository.findById(cureId);
		assertNotNull(cuentaRe,"la cuenta no existe");
		cuentaRegistradaRepository.delete(cuentaRe);
	}
	

}
