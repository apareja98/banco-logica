package co.edu.usbcali.banco.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.TipoUsuario;

@ExtendWith (SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")

@Rollback(false)
class TipoUsuarioRepositoryTest {

	private final static Logger log = LoggerFactory.getLogger(TipoUsuarioRepositoryTest.class);

	private final static Long tiusId= 4L; 
	
	@Autowired
	private TipoUsuarioRepository tipoUsuarioRepository;
	
	
	@DisplayName("Crear tipo de usuario")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	void aTest() {
		TipoUsuario tipoUsuario = new  TipoUsuario();
		tipoUsuario.setTiusId(tiusId);
		tipoUsuario.setNombre("GERENTE");
		tipoUsuario.setActivo("S");
		tipoUsuarioRepository.save(tipoUsuario);
	}
		
	@DisplayName("Consultar por Id")
	@Test
	@Transactional(readOnly=true)
	void bTest() {
	TipoUsuario tiUsuario = tipoUsuarioRepository.findById(tiusId);
	assertNotNull(tiUsuario,"El tipo de usuario no existe");
	}
	
	@DisplayName("Lista todos los tipos de usuario")
	@Test
	@Transactional(readOnly=true)
	void cTest() {
		
		List<TipoUsuario> tiposUsuario = tipoUsuarioRepository.findAll();
		tiposUsuario.forEach(tipoUsuario->{
			log.info(tipoUsuario.getNombre());
			log.info(tipoUsuario.getTiusId().toString());
		});
		
	}
	
	@DisplayName("Actualizar tipo de usuario")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	void dTest() {
		TipoUsuario tiUsuario = tipoUsuarioRepository.findById(tiusId);
		assertNotNull(tiUsuario,"El tipo de usuario no existe");
		tiUsuario.setActivo("N");
		tipoUsuarioRepository.update(tiUsuario);
	}
	
	@DisplayName("eliminar tipo de usuario")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	void eTest() {
		TipoUsuario tiUsuario = tipoUsuarioRepository.findById(tiusId);
		assertNotNull(tiUsuario,"El tipo de usuario no existe");
		tiUsuario.setActivo("N");
		tipoUsuarioRepository.delete(tiUsuario);
	}
	
	
	

}
