package co.edu.usbcali.banco.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.TipoDocumento;


@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(false)
class ClienteRepositoryTest {

	private final static Logger log = LoggerFactory.getLogger(ClienteRepositoryTest.class);
	private final static Long clieId = 142020L;

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;

	@Test
	@DisplayName("CrearCliente")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void aTest() {
		Cliente cliente = new Cliente();
		cliente.setClieId(clieId);
		cliente.setNombre("Homer J Simpson");
		cliente.setActivo("S");
		cliente.setDireccion("Avenida Siempre viva 124");
		cliente.setEmail("homerjsimpson@gmail.com");
		cliente.setTelefono("7-(303)913-5438");
		TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(2L);
		assertNotNull(tipoDocumento, "El tipo de documento es nulo");
		cliente.setTipoDocumento(tipoDocumento);
		clienteRepository.save(cliente);
	}

	@DisplayName("Consultar cliente por id")
	@Test
	@Transactional(readOnly = true)
	void bTest() {

		Cliente cliente = clienteRepository.findById(clieId);
		assertNotNull(cliente, "El cliente no existe");

	}

	@DisplayName("Modificar cliente")
	@Test
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void cTest() {
		Cliente cliente = clienteRepository.findById(clieId);
		assertNotNull(cliente, "El cliente no existe");
		cliente.setDireccion("Calle 423-1232");
		clienteRepository.update(cliente);

	}

	@DisplayName("Eliminar cliente")
	@Test

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void dTest() {

		Cliente cliente = clienteRepository.findById(clieId);
		assertNotNull(cliente, "El cliente no existe");
		cliente.setDireccion("Calle 423-1232");
		clienteRepository.delete(cliente);

	}

	@DisplayName("Todos los clientes")
	@Test
	@Transactional(readOnly = true)
	void eTest() {
		List<Cliente> clientes = clienteRepository.findAll();
		clientes.forEach(cliente -> {
			log.info(cliente.getNombre());
			log.info(cliente.getEmail());
		});
	}

}
