package co.edu.usbcali.banco.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;




@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(false)
class CuentaRepositoryTest {

	private final static Logger log = LoggerFactory.getLogger(CuentaRepositoryTest.class);
	
	@Autowired
	private CuentaRepository cuentaRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	private final static String cuenId= "1234-4326-7647-3678"; 
	private final static Long clieId=3L;
	
	
	@DisplayName("Crear cuenta")
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Test
	void aTest() {
		Cuenta cuenta = new Cuenta();
		cuenta.setCuenId(cuenId);
		cuenta.setActiva("S");
		cuenta.setClave("1234ccs2");
		cuenta.setSaldo(new BigDecimal(635098));
		Cliente cliente = clienteRepository.findById(clieId);
		assertNotNull(cliente,"El cliente es nulo");
		cuenta.setCliente(cliente);
		cuentaRepository.save(cuenta);
	}
	
	@DisplayName("Buscar cuenta")
	@Transactional(readOnly=true)
	@Test
	void bTest() {
		Cuenta cuenta = cuentaRepository.findById(cuenId);
		assertNotNull(cuenta,"La cuenta no existe");
		
	}
	
	@DisplayName("actualizar cuenta")
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Test
	void cTest() {
		Cuenta cuenta = cuentaRepository.findById(cuenId);
		assertNotNull(cuenta,"la cuenta no existe");
		cuenta.setActiva("N");
		cuentaRepository.update(cuenta);
	}
	@DisplayName("Eliminar cuenta")
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Test
	void dTest() {
		Cuenta cuenta = cuentaRepository.findById(cuenId);
		assertNotNull(cuenta,"la cuenta no existe");
		cuentaRepository.delete(cuenta);
	}
	@DisplayName("Listar cuentas")
	@Test
	@Transactional(readOnly=true)
	void eTest() {
		List<Cuenta> cuentas = cuentaRepository.findAll(); 
		cuentas.forEach(cuenta->{
			log.info(cuenta.getCuenId());
		});
	}

}
