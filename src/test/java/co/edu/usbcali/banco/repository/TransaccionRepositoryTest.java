package co.edu.usbcali.banco.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.TipoTransaccion;
import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.domain.Usuario;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(false)
class TransaccionRepositoryTest {
	@Autowired
	private TransaccionRepository transaccionRepository;
	
	@Autowired
	private CuentaRepository cuentaRepository;
	
	@Autowired 
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private TipoTransaccionRepository tipoTransaccionRepository;
	
	private final static Logger log = LoggerFactory.getLogger(ClienteRepositoryTest.class);
	private final static Long tranId  = 3001L;
	private final static String cuenId = "1630-2511-2937-7299";
	private final static String usuUsuario = "aaizikovitz9q";
	private final static Long tiTrId = 2L; 
	
	@Test
	@DisplayName("Crear Transaccion")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void aTest() {
		Transaccion transaccion = new Transaccion();
		transaccion.setTranId(tranId);
		transaccion.setFecha(new Timestamp(new Date().getTime()));
		transaccion.setValor(new BigDecimal(20000));
		TipoTransaccion tiTran = tipoTransaccionRepository.findById(tiTrId);
		assertNotNull(tiTran, "El tipo de transaccion no existe");
		Usuario usuario = usuarioRepository.findById(usuUsuario);
		assertNotNull(usuario, "El usuario no existe");
		Cuenta cuenta =  cuentaRepository.findById(cuenId);
		assertNotNull(cuenta, "La cuenta no existe");
		transaccion.setCuenta(cuenta);
		transaccion.setUsuario(usuario);
		transaccion.setTipoTransaccion(tiTran);
		transaccionRepository.save(transaccion);
	}
	
	@DisplayName("Consultar transaccion por id")
	@Test
	@Transactional(readOnly = true)
	void bTest() {
		Transaccion transaccion = transaccionRepository.findById(tranId);
		assertNotNull(transaccion,"La transaccion es nula");	
	
	}
	
	@DisplayName("Modificar transaccion")
	@Test
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void cTest() {
		Transaccion transaccion = transaccionRepository.findById(tranId);
		assertNotNull(transaccion,"La transaccion es nula");	
		transaccion.setFecha(new Timestamp(new Date().getTime()));
		transaccionRepository.update(transaccion);
	}

	@DisplayName("Eliminar transaccion")
	@Test
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void dTest() {
		Transaccion transaccion = transaccionRepository.findById(tranId);
		assertNotNull(transaccion,"La transaccion es nula");	
		transaccionRepository.delete(transaccion);
	}
	
	@DisplayName("Todas las transacciones")
	@Test
	@Transactional(readOnly = true)
	void eTest() {
		List<Transaccion> transacciones = transaccionRepository.findAll();
		transacciones.forEach(transaccion ->{
			log.info(transaccion.getTranId().toString());
		});
		
	}
}
