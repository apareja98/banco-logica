package co.edu.usbcali.banco.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import co.edu.usbcali.banco.domain.TipoDocumento;
import co.edu.usbcali.banco.services.TipoDocumentoService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")

@Rollback(false)
class TipoDocumentoServiceTest {
	private final static Logger log = LoggerFactory.getLogger(TipoDocumentoServiceTest.class);

	private final static Long tdocId = 4L;

	@Autowired
	private TipoDocumentoService tipoDocumentoService;

	@DisplayName("Crear Tipo de Documento")
	@Test
	void aTest() throws Exception {
		TipoDocumento tipoDocumento = new TipoDocumento();
		tipoDocumento.setTdocId(tdocId);
		tipoDocumento.setNombre("PASAPORTE");
		tipoDocumento.setActivo("S");
		tipoDocumentoService.save(tipoDocumento);
	}
	// findById

	@DisplayName("Buscar tipoDocumento por id")
	@Test
	void bTest() throws Exception {
		TipoDocumento tipoDocumento = tipoDocumentoService.findById(tdocId);
		assertNotNull(tipoDocumento, "El tipo de documento no existe");

	}

	@DisplayName("Buscar Todos los tipos de documento")
	@Test
	void cTest() throws Exception {
		List<TipoDocumento> tiposDocumento = tipoDocumentoService.findAll();
		tiposDocumento.forEach(tiDoc -> {
			log.info(tiDoc.getNombre());
			log.info(tiDoc.getTdocId().toString());
		});
	}

	// update
	@DisplayName("Actualizar Tipo de documento")
	@Test
	void dTest() throws Exception {
		TipoDocumento tipoDocumento = tipoDocumentoService.findById(tdocId);
		assertNotNull(tipoDocumento, "El tipo de documento no existe");
		tipoDocumento.setActivo("N");
		tipoDocumentoService.update(tipoDocumento);

	}
	// remove

	@DisplayName("Eliminar Tipo de documento")
	@Test
	void eTest() throws Exception {
		TipoDocumento tipoDocumento = tipoDocumentoService.findById(tdocId);
		assertNotNull(tipoDocumento, "El tipo de documento no existe");
		tipoDocumentoService.delete(tipoDocumento);

	}

}
