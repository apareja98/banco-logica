package co.edu.usbcali.banco.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.TipoDocumento;
import co.edu.usbcali.banco.services.ClienteService;
import co.edu.usbcali.banco.services.TipoDocumentoService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(false)
class ClienteServiceTest {

	private final static Logger log = LoggerFactory.getLogger(ClienteServiceTest.class);
	private final static Long clieId = 142020L;

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private TipoDocumentoService tipoDocumentoService;

	@Test
	@DisplayName("CrearCliente")
	void aTest() throws Exception {
		Cliente cliente = new Cliente();
		cliente.setClieId(clieId);
		cliente.setNombre("Homer J Simpson");
		cliente.setActivo("S");
		cliente.setDireccion("Avenida Siempre viva 124");
		cliente.setEmail("homerjsimpson@gmail.com");
		cliente.setTelefono("7-(303)913-5438");
		TipoDocumento tipoDocumento = tipoDocumentoService.findById(2L);
		assertNotNull(tipoDocumento, "El tipo de documento es nulo");
		cliente.setTipoDocumento(tipoDocumento);
		clienteService.save(cliente);
	}

	@DisplayName("Consultar cliente por id")
	@Test
	void bTest() throws Exception {

		Cliente cliente = clienteService.findById(clieId);
		assertNotNull(cliente, "El cliente no existe");

	}

	@DisplayName("Modificar cliente")
	@Test
	void cTest() throws Exception {
		Cliente cliente = clienteService.findById(clieId);
		assertNotNull(cliente, "El cliente no existe");
		cliente.setDireccion("Calle 423-1232");
		clienteService.update(cliente);

	}

	@DisplayName("Eliminar cliente")
	@Test
	void dTest() throws Exception {

		Cliente cliente = clienteService.findById(clieId);
		assertNotNull(cliente, "El cliente no existe");
		clienteService.delete(cliente);

	}

	@DisplayName("Todos los clientes")
	@Test
	void eTest() throws Exception {
		List<Cliente> clientes = clienteService.findAll();
		clientes.forEach(cliente -> {
			log.info(cliente.getNombre());
			log.info(cliente.getEmail());
		});
	}
	
	@DisplayName("Login cliente")
	@Test
	void fTest()throws Exception{
		Cliente cliente = clienteService.loginCliente("0000-6776-1365-3228", 572L,"hmKnRrGG");
		log.info(cliente.getNombre());
	}

}
