package co.edu.usbcali.banco.service;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.TipoTransaccion;
import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.services.CuentaService;
import co.edu.usbcali.banco.services.TipoTransaccionService;
import co.edu.usbcali.banco.services.TransaccionService;
import co.edu.usbcali.banco.services.UsuarioService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(false)
class TransaccionServiceTest {

	@Autowired
	private TransaccionService transaccionService;
	@Autowired
	private TipoTransaccionService tipoTransaccionService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private CuentaService cuentaService;

	private final static Logger log = LoggerFactory.getLogger(TransaccionServiceTest.class);
	private static Long tranId =0L;
	private final static String cuenId = "1630-2511-2937-7299";
	private final static String usuUsuario = "aaizikovitz9q";
	private final static Long tiTrId = 2L;

	@Test
	@DisplayName("Crear Transaccion")
	void aTest() throws Exception {
		Transaccion transaccion = new Transaccion();
		//transaccion.setTranId(tranId);
		transaccion.setFecha(new Timestamp(new Date().getTime()));
		transaccion.setValor(new BigDecimal(20000));
		TipoTransaccion tiTran = tipoTransaccionService.findById(tiTrId);
		assertNotNull(tiTran, "El tipo de transaccion no existe");
		Usuario usuario = usuarioService.findById(usuUsuario);
		assertNotNull(usuario, "El usuario no existe");
		Cuenta cuenta = cuentaService.findById(cuenId);
		assertNotNull(cuenta, "La cuenta no existe");
		transaccion.setCuenta(cuenta);
		transaccion.setUsuario(usuario);
		transaccion.setTipoTransaccion(tiTran);
		Transaccion resultado=transaccionService.save(transaccion);
		log.info("Id"+resultado.getTranId());
		tranId=resultado.getTranId();
	}

	@DisplayName("Consultar transaccion por id")
	@Test
	void bTest() throws Exception {
		Transaccion transaccion = transaccionService.findById(tranId);
		assertNotNull(transaccion, "La transaccion es nula");

	}

	@DisplayName("Modificar transaccion")
	@Test
	void cTest() throws Exception {
		Transaccion transaccion = transaccionService.findById(tranId);
		assertNotNull(transaccion, "La transaccion es nula");
		transaccion.setFecha(new Timestamp(new Date().getTime()));
		transaccionService.update(transaccion);
	}

	@DisplayName("Eliminar transaccion")
	@Test
	void dTest() throws Exception {
		Transaccion transaccion = transaccionService.findById(tranId);
		assertNotNull(transaccion, "La transaccion es nula");
		transaccionService.delete(transaccion);
	}

	@DisplayName("Todas las transacciones")
	@Test
	void eTest() throws Exception {
		List<Transaccion> transacciones = transaccionService.findAll();
		transacciones.forEach(transaccion -> {
			log.info(transaccion.getTranId().toString());
		});

	}
	@DisplayName("Transacciones por cliente")
	@Test
	void fTest() throws Exception {
		List<Transaccion> transacciones = transaccionService.transaccionesPorCliente(3001L);
		transacciones.forEach(transaccion -> {
			log.info(transaccion.getTranId().toString());
		});

	}
	
	//"3692-8297-4835-0023"
	@DisplayName("Transacciones por cuenta")
	@Test
	void gTest() throws Exception {
		List<Transaccion> transacciones = transaccionService.transaccionesPorCuenta("3692-8297-4835-0023");
		transacciones.forEach(transaccion -> {
			log.info(transaccion.getTranId().toString());
		});

	}
}
