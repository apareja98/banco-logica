package co.edu.usbcali.banco.service;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import co.edu.usbcali.banco.services.TransaccionBancariaService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(false)
class TransaccionBancariaServiceTest {
	
	@Autowired
	private TransaccionBancariaService transaccionBancariaService;
	
	String cuenId="0000-6776-1365-3228";
	String usuUsuario="aadamoco";
	BigDecimal valor=new BigDecimal(1500000);
	
		String cuenIdDestino="0031-0825-4207-7451";

	@Test
	@DisplayName("Consignar")
	void consignar()throws Exception {
		transaccionBancariaService.consignar(cuenId, usuUsuario, valor);
	}
	
	@Test
	@DisplayName("Retirar")
	void retirar()throws Exception {
		transaccionBancariaService.retirar(cuenId, usuUsuario, valor);
	}
	
	@Test
	@DisplayName("Transferencia")
	void trasferencia()throws Exception {
		transaccionBancariaService.traslado(cuenId, cuenIdDestino, usuUsuario, new BigDecimal(40000));
	}

}
