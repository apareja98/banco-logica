package co.edu.usbcali.banco.service;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.services.TipoUsuarioService;
import co.edu.usbcali.banco.services.UsuarioService;

@ExtendWith (SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")

@Rollback(false)
class UsuarioServiceTest {

	private final static Logger log = LoggerFactory.getLogger(UsuarioServiceTest.class);
	
	private final static String usuUsuario = "apareja1"; 
	
	
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private TipoUsuarioService tipoUsuarioService;
	
	
	
	@DisplayName("CrearUsuario")
	@Test
	void aTest() throws Exception {
		Usuario usuario = new Usuario();
		usuario.setUsuUsuario(usuUsuario);
		usuario.setNombre("Alejandro Pareja");
		usuario.setIdentificacion(new BigDecimal(1155486));
		usuario.setClave("111223sa");
		usuario.setActivo("S");
		TipoUsuario tipoUsuario = tipoUsuarioService.findById(2L);
		assertNotNull(tipoUsuario,"El tipo de usuario no existe");
		usuario.setTipoUsuario(tipoUsuario);
		usuarioService.save(usuario);
		
	}
	
	@DisplayName ("Consultar usuario por id")
	@Test
	void bTest() throws Exception {
		Usuario usuario = usuarioService.findById("apareja1");
		assertNotNull(usuario, "el usuario no existe");

	}
	@DisplayName ("Consultar todos los usuarios")
	@Test
	void cTest() throws Exception {
		List<Usuario> usuarios= usuarioService.findAll();
		usuarios.forEach(usuario ->{
			log.info(usuario.getNombre());
		});
	}
	
	@DisplayName ("Actualizar usuario")
	@Test
	void dTest() throws Exception {
		Usuario usuario = usuarioService.findById("apareja1");
		assertNotNull(usuario, "el usuario no existe");
		usuario.setNombre("Carlos Gutierrez");
		usuarioService.update(usuario);
	}
	
	@DisplayName ("Borrar usuario")
	@Test
	void eTest() throws Exception {
		Usuario usuario = usuarioService.findById("apareja1");
		assertNotNull(usuario, "el usuario no existe");
		usuario.setNombre("Carlos Gutierrez");
		usuarioService.delete(usuario);
	}
	
}
