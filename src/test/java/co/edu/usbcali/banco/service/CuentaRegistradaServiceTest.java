package co.edu.usbcali.banco.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.services.ClienteService;
import co.edu.usbcali.banco.services.CuentaRegistradaService;
import co.edu.usbcali.banco.services.CuentaService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(false)
class CuentaRegistradaServiceTest {

	private final static Logger log = LoggerFactory.getLogger(CuentaRegistradaServiceTest.class);

	@Autowired
	private CuentaRegistradaService cuentaRegistradaService;
	@Autowired
	private CuentaService cuentaService;
	@Autowired
	private ClienteService clienteService;

	private final static Long cureId = 102L;
	private final static Long clieId = 2L;
	private final static String cueId = "3479-1726-5080-7363";

	@DisplayName("Crear cuenta registrada")
	@Test
	void aTest() throws Exception {
		CuentaRegistrada cuentaRe = new CuentaRegistrada();
		cuentaRe.setCureId(null);
		Cliente cliente = clienteService.findById(clieId);
		assertNotNull(cliente, "El cliente es nulo");
		Cuenta cuenta = cuentaService.findById(cueId);
		assertNotNull(cuenta, "la cuenta es nula");
		cuentaRe.setCliente(cliente);
		cuentaRe.setCuenta(cuenta);
		cuentaRegistradaService.save(cuentaRe);
	}

	@DisplayName("Buscar cuenta registrada")

	@Test
	void bTest() throws Exception {
		CuentaRegistrada cuentaRe = cuentaRegistradaService.findById(cureId);
		assertNotNull(cuentaRe, "La cuenta registrada no existe");

	}

	@DisplayName("actualizar cuenta registrada")
	@Test
	void cTest() throws Exception {
		CuentaRegistrada cuentaRe = cuentaRegistradaService.findById(cureId);
		assertNotNull(cuentaRe, "la cuenta no existe");
		Cliente cliente = clienteService.findById(4L);
		assertNotNull(cliente, "El cliente no existe");
		cuentaRe.setCliente(cliente);
		cuentaRegistradaService.update(cuentaRe);
	}

	@DisplayName("Eliminar cuenta registrada")
	@Test
	void dTest() throws Exception {
		CuentaRegistrada cuentaRe = cuentaRegistradaService.findById(cureId);
		assertNotNull(cuentaRe, "la cuenta no existe");
		cuentaRegistradaService.delete(cuentaRe);
	}

	@DisplayName("Listar cuentas registradas")
	@Test
	void eTest() throws Exception {
		List<CuentaRegistrada> cuentasRegistradas = cuentaRegistradaService.findAll();
		cuentasRegistradas.forEach(cuentaRe -> {
			log.info("--------------- Cuentas registradas---------");
			log.info(cuentaRe.getCureId().toString());
		});
	}
	
	

}
