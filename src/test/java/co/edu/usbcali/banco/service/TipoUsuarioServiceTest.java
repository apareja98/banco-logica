package co.edu.usbcali.banco.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.services.TipoUsuarioService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")

@Rollback(false)
class TipoUsuarioServiceTest {

	private final static Logger log = LoggerFactory.getLogger(TipoUsuarioServiceTest.class);

	private final static Long tiusId = 4L;

	@Autowired
	private TipoUsuarioService tipoUsuarioService;

	@DisplayName("Crear tipo de usuario")
	@Test
	void aTest() throws Exception {
		TipoUsuario tipoUsuario = new TipoUsuario();
		tipoUsuario.setTiusId(tiusId);
		tipoUsuario.setNombre("GERENTE");
		tipoUsuario.setActivo("S");
		tipoUsuarioService.save(tipoUsuario);
	}

	@DisplayName("Consultar por Id")
	@Test
	void bTest() throws Exception {
		TipoUsuario tiUsuario = tipoUsuarioService.findById(tiusId);
		assertNotNull(tiUsuario, "El tipo de usuario no existe");
	}

	@DisplayName("Lista todos los tipos de usuario")
	@Test
	void cTest() throws Exception {

		List<TipoUsuario> tiposUsuario = tipoUsuarioService.findAll();
		tiposUsuario.forEach(tipoUsuario -> {
			log.info(tipoUsuario.getNombre());
			log.info(tipoUsuario.getTiusId().toString());
		});

	}

	@DisplayName("Actualizar tipo de usuario")
	@Test
	void dTest() throws Exception {
		TipoUsuario tiUsuario = tipoUsuarioService.findById(tiusId);
		assertNotNull(tiUsuario, "El tipo de usuario no existe");
		tiUsuario.setActivo("N");
		tipoUsuarioService.update(tiUsuario);
	}

	@DisplayName("eliminar tipo de usuario")
	@Test
	void eTest() throws Exception {
		TipoUsuario tiUsuario = tipoUsuarioService.findById(tiusId);
		assertNotNull(tiUsuario, "El tipo de usuario no existe");
		tiUsuario.setActivo("N");
		tipoUsuarioService.delete(tiUsuario);
	}

}
