package co.edu.usbcali.banco.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import co.edu.usbcali.banco.domain.TipoTransaccion;
import co.edu.usbcali.banco.services.TipoTransaccionService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")

@Rollback(false)
class TipoTransaccionServiceTest {

	private final static Logger log = LoggerFactory.getLogger(TipoTransaccionServiceTest.class);

	private final static Long titrId = 4L;

	@Autowired
	private TipoTransaccionService tipoTransaccionService;

	@DisplayName("Crear tipo de transaccion")
	@Test
	void aTest() throws Exception {
		TipoTransaccion tipoTransaccion = new TipoTransaccion();
		tipoTransaccion.setTitrId(titrId);
		tipoTransaccion.setNombre("RECAUDO");
		tipoTransaccion.setActivo("S");
		tipoTransaccionService.save(tipoTransaccion);
	}

	@DisplayName("Consultar tipoTransaccion por Id")
	@Test
	void bTest() throws Exception {
		TipoTransaccion tiTransaccion = tipoTransaccionService.findById(titrId);
		Object object = new Object();
		object = tiTransaccion;
		log.info("---------fsdfsdfsd---" + object.getClass().toString());
		assertNotNull(tiTransaccion, "El tipo de transaccion no existe");
	}

	@DisplayName("Listar todos los tipos de transaccion")
	@Test
	void cTest() throws Exception {

		List<TipoTransaccion> tiposTransaccion = tipoTransaccionService.findAll();
		tiposTransaccion.forEach(tipoUsuario -> {
			log.info(tipoUsuario.getNombre());
			log.info(tipoUsuario.getTitrId().toString());
		});

	}

	@DisplayName("Actualizar tipo de transaccion")
	@Test
	void dTest() throws Exception {
		TipoTransaccion tipoTransaccion = tipoTransaccionService.findById(titrId);
		assertNotNull(tipoTransaccion, "El tipo de transacción no existe");
		tipoTransaccion.setActivo("N");
		tipoTransaccionService.update(tipoTransaccion);
	}

	@DisplayName("eliminar tipo de Transacción")
	@Test
	void eTest() throws Exception {
		TipoTransaccion tipoTransaccion = tipoTransaccionService.findById(titrId);
		assertNotNull(tipoTransaccion, "El tipo de transacción no existe");
		tipoTransaccion.setActivo("N");
		tipoTransaccionService.delete(tipoTransaccion);
	}

}
