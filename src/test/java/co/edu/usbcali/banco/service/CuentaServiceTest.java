package co.edu.usbcali.banco.service;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.services.ClienteService;
import co.edu.usbcali.banco.services.CuentaService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(false)
class CuentaServiceTest {

	private final static Logger log = LoggerFactory.getLogger(CuentaServiceTest.class);

	@Autowired
	private CuentaService cuentaService;
	@Autowired
	private ClienteService clienteService;
	private final static String cuenId = "1234-4326-7647-3678";
	private final static Long clieId = 3L;

	@DisplayName("Crear cuenta")
	@Test
	void aTest() throws Exception {
		Cuenta cuenta = new Cuenta();
		cuenta.setCuenId(cuenId);
		cuenta.setActiva("S");
		cuenta.setClave("1234ccs2");
		cuenta.setSaldo(new BigDecimal(635098));
		Cliente cliente = clienteService.findById(clieId);
		assertNotNull(cliente, "El cliente es nulo");
		cuenta.setCliente(cliente);
		cuentaService.save(cuenta);
	}

	@DisplayName("Buscar cuenta")
	@Test
	void bTest() throws Exception {
		Cuenta cuenta = cuentaService.findById(cuenId);
		assertNotNull(cuenta, "La cuenta no existe");

	}

	@DisplayName("actualizar cuenta")
	@Test
	void cTest() throws Exception {
		Cuenta cuenta = cuentaService.findById(cuenId);
		assertNotNull(cuenta, "la cuenta no existe");
		cuenta.setActiva("N");
		cuentaService.update(cuenta);
	}

	@DisplayName("Eliminar cuenta")
	@Test
	void dTest() throws Exception {
		Cuenta cuenta = cuentaService.findById(cuenId);
		assertNotNull(cuenta, "la cuenta no existe");
		cuentaService.delete(cuenta);
	}

	@DisplayName("Listar cuentas")
	@Test
	void eTest() throws Exception {
		List<Cuenta> cuentas = cuentaService.findAll();
		cuentas.forEach(cuenta -> {
			log.info(cuenta.getCuenId());
		});
	}
	
	@DisplayName("Cuentas por cliente")
	@Test
	void fTest() throws Exception {
		List<Cuenta> cuentas = cuentaService.cuentasPorCliente(3001L);
		cuentas.forEach(cuenta -> {
			log.info(cuenta.getCuenId());
		});
	}

}
