package co.edu.usbcali.banco.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith (SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
class UsuarioTest {

	private final static Logger log = LoggerFactory.getLogger(UsuarioTest.class);
	
	private final static String usuUsuario = "apareja1"; 
	
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@DisplayName("CrearUsuario")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	void aTest() {
		Usuario usuario = new Usuario();
		usuario.setUsuUsuario(usuUsuario);
		usuario.setNombre("Alejandro Pareja");
		usuario.setIdentificacion(new BigDecimal(1155486));
		usuario.setClave("111223sa");
		usuario.setActivo("S");
		TipoUsuario tipoUsuario = entityManager.find(TipoUsuario.class, 2L);
		assertNotNull(tipoUsuario,"El tipo de usuario no existe");
		usuario.setTipoUsuario(tipoUsuario);
		entityManager.persist(usuario);
		
	}
	
	@Transactional(readOnly=true)
	@DisplayName ("Consultar usuario por id")
	@Test
	void bTest() {
		Usuario usuario = entityManager.find(Usuario.class, "apareja1");
		assertNotNull(usuario);

	}
	@Transactional(readOnly=true)
	@DisplayName ("Consultar todos los usuarios")
	@Test
	void cTest() {
		String JPQL =" Select usu from Usuario usu";
		List<Usuario> usuarios = entityManager.createQuery(JPQL).getResultList();
		usuarios.forEach(usuario ->{
			log.info(usuario.getNombre());
		});
	}
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	@DisplayName ("Actualizar usuario")
	@Test
	void dTest() {
		Usuario usuario = entityManager.find(Usuario.class, "apareja1");
		assertNotNull(usuario);
		usuario.setNombre("Carlos Gutierrez");
		entityManager.merge(usuario);
	}
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	@DisplayName ("Borrar usuario")
	@Test
	void eTest() {
		Usuario usuario = entityManager.find(Usuario.class, "apareja1");
		assertNotNull(usuario);
		usuario.setNombre("Carlos Gutierrez");
		entityManager.remove(usuario);
	}
	@Transactional(readOnly=true)
	@DisplayName("JPQLUsuarios")
	@Test
	void fTest() {
		String JPQL = "Select usu from Usuario usu where usu.activo ='S' and usu.tipoUsuario.tiusId=1L";
		List<Usuario> usuarios = entityManager.createQuery(JPQL).getResultList();
		usuarios.forEach(usuario->{
			log.info(usuario.getNombre());
		});
	}
}
