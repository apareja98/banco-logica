package co.edu.usbcali.banco.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@ExtendWith (SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
class ClienteTest {
	
	
	private final static Logger log = LoggerFactory.getLogger(ClienteTest.class);
	private final static Long clieId = 142020L; 
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	@Test
	@DisplayName("CrearCliente")
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	void aTest() {
		
		Cliente cliente = new Cliente();
		cliente.setClieId(clieId);
		cliente.setNombre("Homer J Simpson");
		cliente.setActivo("S");
		cliente.setDireccion("Avenida Siempre viva 124");
		cliente.setEmail("homerjsimpson@gmail.com");
		cliente.setTelefono("7-(303)913-5438");
		
		TipoDocumento tipoDocumento = entityManager.find(TipoDocumento.class, 2L);
		assertNotNull(tipoDocumento,"El tipo de documento es nulo");
		cliente.setTipoDocumento(tipoDocumento);
		entityManager.persist(cliente);
		
	}
	
	@DisplayName("Consultar cliente por id")
	@Test
	@Transactional(readOnly=true)
	void bTest() {
		
		Cliente cliente = entityManager.find(Cliente.class, clieId);
		assertNotNull(cliente,"El cliente no existe");
		
		
	}
	
	@DisplayName("Modificar cliente")
	@Test
	@Rollback(false)
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	void cTest() {		
		Cliente cliente = entityManager.find(Cliente.class, clieId);
		assertNotNull(cliente,"El cliente no existe");
		cliente.setDireccion("Calle 423-1232");
		entityManager.merge(cliente);
		
		
	}
	@DisplayName("Eliminar cliente")
	@Test
	@Rollback(false)
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	void dTest() {
		
		Cliente cliente = entityManager.find(Cliente.class, clieId);
		assertNotNull(cliente,"El cliente no existe");
		cliente.setDireccion("Calle 423-1232");
		entityManager.remove(cliente);
		
	}
	@DisplayName("Todos los clientes")
	@Test
	@Transactional(readOnly=true)
	void eTest() {
		String JPQL =" SELECT cli from Cliente cli";
		List<Cliente> clientes = entityManager.createQuery(JPQL).getResultList();
		clientes.forEach(cliente ->{
			log.info(cliente.getNombre());
			log.info(cliente.getEmail());
		});
	}
	
}
