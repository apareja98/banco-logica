package co.edu.usbcali.banco.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import co.edu.usbcali.banco.dto.CuentaSaldoDTO;

@ExtendWith (SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")

class JPQLTest {
	private final static Logger log = LoggerFactory.getLogger(JPQLTest.class);
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@DisplayName("Consultar cliente con where")
	@Test()
	@Transactional(readOnly=true)
	void selectWhere() {
		String JQPL ="SELECT cli from Cliente cli where cli.tipoDocumento.tdocId=2";
		List<Cliente> clientes = entityManager.createQuery(JQPL).getResultList();
		clientes.forEach(cliente ->{
			log.info(cliente.getNombre());
			log.info(cliente.getEmail());
		});
	}

	@Transactional(readOnly=true)
	@DisplayName("Contar clientes")
	@Test()
	void selectAritmetica() {
	String JPQL ="SELECT COUNT(cli) from Cliente cli where cli.tipoDocumento.tdocId=2 and cli.activo='S'";
		Long count = (Long)entityManager.createQuery(JPQL).getSingleResult();
		log.info(count.toString());
	}

	@DisplayName("Clientes con mas de una cuenta")
	@Test
	@Transactional(readOnly=true)
	void fTest() {
		String JPQL ="SELECT cli  from Cliente cli where size(cli.cuentas) > 1";
		List<Cliente> clientes = entityManager.createQuery(JPQL).getResultList();
		clientes.forEach(cliente->{
			log.info(cliente.getEmail());
			log.info(cliente.getNombre());
			log.info("---");
		});
	}
	@DisplayName("ConsultarCuentasSaldos")
	@Test
	@Transactional(readOnly=true)
	void selectCuentasSaldos() {
		String JPQL ="SELECT COUNT(cue),MIN(cue.saldo),MAX(cue.saldo),AVG(cue.saldo) FROM Cuenta cue";
		Object []resultado = (Object[]) entityManager.createQuery(JPQL).getSingleResult();
		Long count = (Long)resultado[0];
		BigDecimal min = (BigDecimal) resultado[1];
		BigDecimal max = (BigDecimal) resultado[2];
		Double avg = (Double)resultado[3];
		log.info(count.toString());
		log.info(min.toString());
		log.info(max.toString());
		log.info(avg.toString());
	}
	@DisplayName("ConsultarCuentasSaldosDTO")
	@Test
	@Transactional(readOnly=true)
	void selectCuentasSaldosDTO() {
		String JPQL ="SELECT new co.edu.usbcali.banco.dto.CuentaSaldoDTO(COUNT(cue),MIN(cue.saldo),MAX(cue.saldo),AVG(cue.saldo)) FROM Cuenta cue";
		CuentaSaldoDTO cuentaSaldoDTO = (CuentaSaldoDTO) entityManager.createQuery(JPQL).getSingleResult();
		log.info(cuentaSaldoDTO.getCount().toString());
		log.info(cuentaSaldoDTO.getMin().toString());
		log.info(cuentaSaldoDTO.getMax().toString());
		log.info(cuentaSaldoDTO.getAvg().toString());
	}
}
