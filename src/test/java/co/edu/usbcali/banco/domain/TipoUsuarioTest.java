package co.edu.usbcali.banco.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith (SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
class TipoUsuarioTest {

	private final static Logger log = LoggerFactory.getLogger(TipoUsuarioTest.class);

	private final static Long tiusId= 4L; 
	

	@PersistenceContext
	private EntityManager entityManager;
	
	@DisplayName("Crear tipo de usuario")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	void aTest() {
		TipoUsuario tipoUsuario = new  TipoUsuario();
		tipoUsuario.setTiusId(tiusId);
		tipoUsuario.setNombre("GERENTE");
		tipoUsuario.setActivo("S");
		entityManager.persist(tipoUsuario);
	}
		
	@DisplayName("Consultar por Id")
	@Test
	@Transactional(readOnly=true)
	void bTest() {
	TipoUsuario tiUsuario = entityManager.find(TipoUsuario.class, tiusId);
	assertNotNull(tiUsuario,"El tipo de usuario no existe");
	}
	
	@DisplayName("Lista todos los tipos de usuario")
	@Test
	@Transactional(readOnly=true)
	void cTest() {
		String JPQL ="Select tiUs from TipoUsuario tiUs";
		List<TipoUsuario> tiposUsuario = entityManager.createQuery(JPQL).getResultList();
		tiposUsuario.forEach(tipoUsuario->{
			log.info(tipoUsuario.getNombre());
			log.info(tipoUsuario.getTiusId().toString());
		});
		
	}
	
	@DisplayName("Actualizar tipo de usuario")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	void dTest() {
		TipoUsuario tiUsuario = entityManager.find(TipoUsuario.class, tiusId);
		assertNotNull(tiUsuario,"El tipo de usuario no existe");
		tiUsuario.setActivo("N");
		entityManager.merge(tiUsuario);
	}
	
	@DisplayName("eliminar tipo de usuario")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	void eTest() {
		TipoUsuario tiUsuario = entityManager.find(TipoUsuario.class, tiusId);
		assertNotNull(tiUsuario,"El tipo de usuario no existe");
		tiUsuario.setActivo("N");
		entityManager.remove(tiUsuario);
	}
	
	@DisplayName("Consulta con Where")
	@Test
	@Transactional(readOnly=true)
	void fTest() {
		String JPQL ="Select tiUs from TipoUsuario tiUs where tiUs.activo ='S'";
		List<TipoUsuario> tiposUsuario = entityManager.createQuery(JPQL).getResultList();
		tiposUsuario.forEach(tipoUsuario->{
			log.info(tipoUsuario.getNombre());
			log.info(tipoUsuario.getTiusId().toString());
		});
		
	}

	

}
