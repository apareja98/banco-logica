package co.edu.usbcali.banco.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@ExtendWith (SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")

class TipoDocumentoTest {
	private final static Logger log = LoggerFactory.getLogger(TipoDocumentoTest.class);
	
	private final static Long tdocId = 4L; 
	
	@PersistenceContext
	private EntityManager entityManager;
	

	@DisplayName("Crear Tipo de Documento")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	void aTest() {
		TipoDocumento tipoDocumento = new TipoDocumento();
		tipoDocumento.setTdocId(tdocId);
		tipoDocumento.setNombre("PASAPORTE");
		tipoDocumento.setActivo("S");
		entityManager.persist(tipoDocumento);
	}
	//findById
	
	@DisplayName("Buscar por id")
	@Test
	@Transactional(readOnly=true)
		void bTest() {
		TipoDocumento tipoDocumento = entityManager.find(TipoDocumento.class, tdocId);
		assertNotNull(tipoDocumento,"El tipo de documento no existe");
		
	}
	//readAll
	@DisplayName("Buscar Todos los tipos de documento")
	@Test
	@Transactional(readOnly=true)
	void cTest() {
		String JPQL = "Select tido from TipoDocumento tido";
		List<TipoDocumento> tiposDocumento = entityManager.createQuery(JPQL).getResultList();
		tiposDocumento.forEach(tiDoc->{
			log.info(tiDoc.getNombre());
			log.info(tiDoc.getTdocId().toString());
		});
	}
	
	//update
	@DisplayName("Actualizar Tipo de documento")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	void dTest() {
		TipoDocumento tipoDocumento = entityManager.find(TipoDocumento.class, tdocId);
		assertNotNull(tipoDocumento,"El tipo de documento no existe");
		tipoDocumento.setActivo("N");
		entityManager.merge(tipoDocumento);
		
	}
	//remove
	
	@DisplayName("Eliminar Tipo de documento")
	@Test
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	void eTest() {
		TipoDocumento tipoDocumento = entityManager.find(TipoDocumento.class, tdocId);
		assertNotNull(tipoDocumento,"El tipo de documento no existe");
		tipoDocumento.setActivo("N");
		entityManager.remove(tipoDocumento);
		
		
	}
	//CustomQuery
	@DisplayName("JPQL Tipo de documento")
	@Test
	@Transactional(readOnly=true)
	void fTest() {
		String JPQL = "Select tido from TipoDocumento tido where tido.activo = 'S'";
		List<TipoDocumento> tiposDocumento = entityManager.createQuery(JPQL).getResultList();
		tiposDocumento.forEach(tiDoc->{
			log.info(tiDoc.getTdocId().toString());
			log.info(tiDoc.getNombre());
		});
	}
}
