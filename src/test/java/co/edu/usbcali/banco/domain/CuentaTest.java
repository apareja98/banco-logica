package co.edu.usbcali.banco.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith (SpringExtension.class)
@ContextConfiguration("/applicationContext.xml")
class CuentaTest {

	private final static Logger log = LoggerFactory.getLogger(CuentaTest.class);
	private final static String cuenId= "1234-4326-7647-3678"; 
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	
	@Transactional(readOnly=true)
	@DisplayName("Seleccionar todas las cuentas")
	@Test
	void aTest() {
		String JPQL ="Select cuenta from Cuenta cuenta";
		List<Cuenta> cuentas = entityManager.createQuery(JPQL).getResultList();
		cuentas.forEach(cuenta->{
			log.info(cuenta.getCuenId());
		});
	}
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Rollback(false)
	@DisplayName("CrearCuenta")
	@Test
	void bTest() {
		Cuenta cuenta = new Cuenta();
		cuenta.setCuenId(cuenId);
		cuenta.setClave("Lx233Ms");
		cuenta.setSaldo(new BigDecimal(2000000));
		cuenta.setActiva("S");
		Cliente cliente = entityManager.find(Cliente.class, 2L);
		assertNotNull(cliente,"El cliente no existe");
		cuenta.setCliente(cliente);
		
		entityManager.persist(cuenta);
		
	}
	
	@DisplayName("Buscar Cuenta Por Id")
	@Test
	@Transactional(readOnly=true)
	void cTest() {
		Cuenta cuenta = entityManager.find(Cuenta.class, cuenId);
		assertNotNull(cuenta,"La cuenta no existe");
		
	}
	
	@DisplayName("Actualizar cuenta")
	@Test
	@Rollback(false)
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	void dTest() {
		Cuenta cuenta = entityManager.find(Cuenta.class, cuenId);
		assertNotNull(cuenta,"La cuenta no existe");
		cuenta.setClave("asfsMasc12");
		cuenta.setSaldo(new BigDecimal(1000000));
		entityManager.merge(cuenta);
		
	}
	
	@DisplayName("Eliminar cuenta")
	@Test
	@Rollback(false)
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	
	void eTest() {
		Cuenta cuenta = entityManager.find(Cuenta.class, cuenId);
		assertNotNull(cuenta,"La cuenta no existe");
		cuenta.setClave("asfsMasc12");
		cuenta.setSaldo(new BigDecimal(1000000));
		
		entityManager.remove(cuenta);
		
	}
	
	@DisplayName("Cuenta con where")
	@Test
	@Transactional(readOnly=true)
	void fTest() {
		String JPQL ="Select cuen from Cuenta cuen where cuen.activa = 'S' and cuen.cliente.tipoDocumento.tdocId = 2L ";
		List<Cuenta> cuentas = entityManager.createQuery(JPQL).getResultList();
		cuentas.forEach(cuenta->{
			log.info(cuenta.getCuenId());
		});
	}
	
	
	
	
}
